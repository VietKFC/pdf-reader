package com.example.pdfreader;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.util.Arrays;
import java.util.List;

public class Constants {

    public static final int ERASER_TOOL = 0;
    public static final int PAN_TOOL = 1;
    public static final int BRUSH_TOOL = 2;
    public static final int SIGNATURE = 3;
    public static final int NOTE_TOOL = 4;
    public static final int TEXT_TOOL = 5;
    public static final int IMAGE_TOOL = 6;
    public static final int MULTI_TOOL = 7;
    public static final int FREEHL_TOOL = 8;
    public static final int RECTANGLE_TOOL = 9;
    public static final int LINE_TOOL = 10;
    public static final int STAMP_TOOL = 11;
    public static final int HIGHLIGHT_TOOL = 12;
    public static final int UNDERLINE_TOOL = 13;
    public static final int SQUIGGLY_TOOL = 14;
    public static final int STRIKEOUT_TOOL = 15;

    public static final String PDF = "PDF";
    public static final String DOC = "Doc";
    public static final String ALL = "All";
    public static final String IMAGE = "Image";

    public static final List<String> SPECIAL_CHARS = Arrays.asList("/", "*", "<", ">", "?", "|", ":");

    public static final int EDIT = 1;
    public static final int COMPRESS = 2;
    public static final int SHARE = 3;
    public static final int FAVOURITE = 4;
    public static final int RENAME = 5;
    public static final int DUPLICATE = 6;
    public static final int DELETE = 7;
    public static final int DOWNLOAD = 8;
    public static final int SAVE_AS_NEW_FILE = 9;

    public static int getAppVersionCode(Context mContext) {
        int versionCode = 0;
        try {
            PackageInfo packageInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

}
