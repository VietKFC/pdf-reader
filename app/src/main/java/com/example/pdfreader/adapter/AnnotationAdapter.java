package com.example.pdfreader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pdfreader.AnnotationEventCallBack;
import com.example.pdfreader.R;
import com.example.pdfreader.model.AnnotModel;

import java.util.List;

public class AnnotationAdapter extends RecyclerView.Adapter<AnnotationAdapter.ViewHolder> {
    private List<AnnotModel> annotList;
    private Context context;
    private AnnotationEventCallBack callBack;

    public AnnotationAdapter(Context context) {
        this.context = context;
    }

    public void setCallBack(AnnotationEventCallBack callBack) {
        this.callBack = callBack;
    }

    public void setAnnotList(List<AnnotModel> annotList) {
        this.annotList = annotList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(context).inflate(R.layout.annotation_item, parent, false), context);
    }

    @Override
    public void onBindViewHolder(@NonNull AnnotationAdapter.ViewHolder holder, int position) {
        AnnotModel annotModel = annotList.get(position);
        if (annotModel != null) {
            holder.bindView(annotModel, callBack);
        }
    }

    @Override
    public int getItemCount() {
        return annotList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivAnnot;
        private TextView tvAnnot;
        private Context context;

        public ViewHolder(@NonNull View itemView, Context context) {
            super(itemView);

            tvAnnot = itemView.findViewById(R.id.tv_annot_item);
            ivAnnot = itemView.findViewById(R.id.iv_annot_item);
            this.context = context;
        }

        public void bindView(AnnotModel annotModel, AnnotationEventCallBack callBack) {
            tvAnnot.setText(annotModel.getAnnotName());
            ivAnnot.setImageResource(annotModel.getAnnotImg());
            if (annotModel.isSelected()) {
                ivAnnot.setColorFilter(context.getResources().getColor(R.color.colorRed));
                tvAnnot.setTextColor(context.getResources().getColor(R.color.colorRed));
            }else {
                ivAnnot.setColorFilter(context.getResources().getColor(R.color.black));
                tvAnnot.setTextColor(context.getResources().getColor(R.color.black));
            }

            ivAnnot.setOnClickListener(view -> {
                if (callBack != null) {
                    callBack.onClickAnnotation(annotModel);
                }
            });
        }

    }
}
