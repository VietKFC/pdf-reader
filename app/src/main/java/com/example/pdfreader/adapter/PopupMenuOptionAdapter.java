package com.example.pdfreader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pdfreader.R;
import com.example.pdfreader.model.DocumentOptionModel;
import com.example.pdfreader.utils.AppPref;

import java.util.List;

public class PopupMenuOptionAdapter extends RecyclerView.Adapter<PopupMenuOptionAdapter.ViewHolder> {

    private Context context;
    private List<DocumentOptionModel> documentOptionModels;
    private OnClickPopupMenuOption onClickPopupMenuOption;
    private AppPref appPref;

    public PopupMenuOptionAdapter(Context context) {
        this.context = context;
        this.appPref = AppPref.instance(context);
    }

    public void setDocumentOptionModels(List<DocumentOptionModel> documentOptionModels) {
        this.documentOptionModels = documentOptionModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PopupMenuOptionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.doc_option_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DocumentOptionModel documentOptionModel = this.documentOptionModels.get(position);
        if (documentOptionModel != null) {

            if (position == 1) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) holder.constraintLayout.getLayoutParams();
                layoutParams.setMargins(0, 10, 0, 0);
                holder.constraintLayout.setLayoutParams(layoutParams);
            }
            if (position <= 2) {
                holder.ivOption.setVisibility(View.VISIBLE);
            } else {
                holder.ivOption.setVisibility(View.INVISIBLE);
            }

            if (position == 2) {
                holder.constraintLayout.setEnabled(false);
                holder.ivOption.setVisibility(View.INVISIBLE);
                holder.tvOptionName.setVisibility(View.GONE);
                holder.tvOptionType.setVisibility(View.GONE);
                holder.straghtLine.setVisibility(View.VISIBLE);
            } else {
                holder.tvOptionName.setVisibility(View.VISIBLE);
                holder.tvOptionType.setVisibility(View.VISIBLE);
                holder.straghtLine.setVisibility(View.GONE);
            }

            this.checkFileTypePosition(position, holder);

            if (position == 3) {
                holder.constraintLayout.setEnabled(false);
                holder.tvOptionType.setText("File type");
                holder.tvOptionType.setVisibility(View.VISIBLE);
                holder.tvOptionName.setVisibility(View.GONE);
            } else {
                holder.tvOptionType.setVisibility(View.GONE);
                holder.constraintLayout.setEnabled(true);
            }

            holder.ivOption.setImageResource(documentOptionModel.getOptionImg());
            holder.tvOptionName.setText(documentOptionModel.getOptionName());
            if (position != 2 && position != 3) {
                holder.constraintLayout.setOnClickListener(v -> {
                    if (this.onClickPopupMenuOption != null) {
                        if (position >= 4 && position <= 7) {
                            this.onClickPopupMenuOption.onClickMenuItemFileType(position);
                        }
                        if (position == 0) {
                            this.onClickPopupMenuOption.onClickEventRefreshData();
                        }

                        if (position == 1) {
                            this.onClickPopupMenuOption.onClickEventSettingFragment();
                        }

                        this.onClickPopupMenuOption.onClosePopupMenu();
                    }
                });
            }
        }
    }

    public void checkFileTypePosition(int position, ViewHolder holder) {
        if (position == this.appPref.getFileTypeOptionPosition()) {
            holder.ivOption.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return this.documentOptionModels == null ? 0 : this.documentOptionModels.size();
    }

    public void setPopupMenuCallBack(OnClickPopupMenuOption callBack) {
        this.onClickPopupMenuOption = callBack;
    }

    public interface OnClickPopupMenuOption {

        void onClickMenuItemFileType(int type);

        void onClickEventRefreshData();

        void onClickEventSettingFragment();

        void onClosePopupMenu();


    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivOption;
        TextView tvOptionName;
        TextView tvOptionType;
        View straghtLine;
        ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivOption = itemView.findViewById(R.id.iv_checked_option);
            tvOptionName = itemView.findViewById(R.id.tv_option_name);
            tvOptionType = itemView.findViewById(R.id.tv_option_type);
            straghtLine = itemView.findViewById(R.id.view_straight_item);
            constraintLayout = itemView.findViewById(R.id.ctl_option_item);
        }
    }
}
