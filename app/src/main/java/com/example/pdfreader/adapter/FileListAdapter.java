package com.example.pdfreader.adapter;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.pdfreader.R;
import com.example.pdfreader.databinding.FileMainItemBinding;
import com.example.pdfreader.model.FileModel;
import com.example.pdfreader.utils.FileUtils;
import com.example.pdfreader.utils.ThumbnailBookLoading;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FileListAdapter extends RecyclerView.Adapter<FileListAdapter.ViewHolder> {

    private List<FileModel> fileModelList;
    private Context context;
    OnEventToActivity callback;

    public void setCallback(OnEventToActivity callback) {
        this.callback = callback;
    }

    public FileListAdapter(Context context) {
        this.context = context;
    }


    public void setFileModelList(List<FileModel> fileModelList) {
        this.fileModelList = fileModelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FileListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FileMainItemBinding binding = FileMainItemBinding.inflate(LayoutInflater.from(context) , parent , false);
        View view = binding.getRoot();
        return new ViewHolder(view , binding , context);
    }

    @Override
    public void onBindViewHolder(@NonNull FileListAdapter.ViewHolder holder, int position) {
        FileModel fileModel = this.fileModelList.get(position);
        if(fileModel != null){
            holder.bindView(fileModel , position , callback);
        }
    }

    @Override
    public int getItemCount() {
        if(fileModelList == null){
            return 0;
        }
        return fileModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private FileMainItemBinding binding;
        private Context context;

        public ViewHolder(@NonNull View itemView , FileMainItemBinding binding , Context context) {
            super(itemView);
            this.binding = binding;
            this.context = context;
        }

        public void bindView(FileModel fileModel, int position , OnEventToActivity callback) {
            binding.tvFileInfor.setText(String.format(context.getString(R.string.file_infor), fileModel.getMimeType() , FileUtils.convertBytes((long)fileModel.getSize())));
            binding.tvFileName.setText(fileModel.getFilePath().substring(fileModel.getFilePath().lastIndexOf("/") + 1));


            new ThumbnailBookLoading(context , fileModel.getFilePath()).excute()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Pair<String, Boolean>>() {
                        @Override
                        public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                        }

                        @Override
                        public void onSuccess(@io.reactivex.annotations.NonNull Pair<String, Boolean> stringBooleanPair) {
                            Glide.with(context).load(stringBooleanPair.first)
                                    .thumbnail(0.1f)
                                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                    .error(R.drawable.book_default_rectangle_vector)
                                    .into(binding.ivMainFile);
                        }

                        @Override
                        public void onError(@io.reactivex.annotations.NonNull Throwable e) {

                        }
                    });
            binding.clFile.setOnClickListener(view -> {
                if(callback != null){
                    callback.startDocumentActivity(fileModel.getFilePath());
                }
            });

            binding.ivMenuItem.setOnClickListener(view -> callback.onClickMenu(fileModel));
        }
    }

    public interface OnEventToActivity{
        void startDocumentActivity(String path);
        void onClickMenu(FileModel fileModel);
    }
}
