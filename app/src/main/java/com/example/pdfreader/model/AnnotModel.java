package com.example.pdfreader.model;

public class AnnotModel {

    private String annotName;
    private int annotImg;
    private int annotType;
    private boolean isSelected;

    public AnnotModel(String annotName, int annotImg, int annotType) {
        this.annotName = annotName;
        this.annotImg = annotImg;
        this.annotType = annotType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getAnnotName() {
        return annotName;
    }

    public void setAnnotName(String annotName) {
        this.annotName = annotName;
    }

    public int getAnnotImg() {
        return annotImg;
    }

    public void setAnnotImg(int annotImg) {
        this.annotImg = annotImg;
    }

    public int getAnnotType() {
        return annotType;
    }

    public void setAnnotType(int annotType) {
        this.annotType = annotType;
    }
}
