package com.example.pdfreader.model;

import java.util.Date;

public class FileModel {

    private String filePath;
    private double size;
    private Date date;
    private String mimeType;

    public FileModel(String filePath, double size, Date date, String mimeType) {
        this.filePath = filePath;
        this.size = size;
        this.date = date;
        this.mimeType = mimeType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public double getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
