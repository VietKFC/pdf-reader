package com.example.pdfreader.model;

public class DocumentOptionModel {

    private int optionImg;
    private int optionType;
    private String optionName;

    public DocumentOptionModel(int optionImg, int optionType, String optionName) {
        this.optionImg = optionImg;
        this.optionType = optionType;
        this.optionName = optionName;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public int getOptionImg() {
        return optionImg;
    }

    public void setOptionImg(int optionImg) {
        this.optionImg = optionImg;
    }


    public int getOptionType() {
        return optionType;
    }

    public void setOptionType(int optionType) {
        this.optionType = optionType;
    }
}
