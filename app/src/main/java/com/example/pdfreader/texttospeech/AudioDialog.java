package com.example.pdfreader.texttospeech;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.pdfreader.R;
import com.example.pdfreader.utils.AppPref;

public class AudioDialog extends Dialog implements View.OnClickListener {

    private SeekBar rateSeekbar;
    private SeekBar pitchSeekbar;
    private TextToSpeech tts;
    private TextView ratePercent;
    private TextView pitchPercent;
    private ConstraintLayout languageCtl;
    private OnEventDocumentActivity callBack;

    public AudioDialog(@NonNull Context context, TextToSpeech tts ) {
        super(context);
        this.tts = tts;
    }

    public static float convertProgress(int progress) {
        return 2 * progress / 100f;
    }

    public void setCallBack(OnEventDocumentActivity callBack) {
        this.callBack = callBack;
    }

    @Override
    public void show() {
        super.show();
        rateSeekbar.setProgress(AppPref.instance(getContext()).getSpeed());
        pitchSeekbar.setProgress(AppPref.instance(getContext()).getPitch());
        ratePercent.setText(AppPref.instance(getContext()).getSpeed() + "%");
        pitchPercent.setText(AppPref.instance(getContext()).getPitch() + "%");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_audio, null, false);
        initDialog(view);
        seekbarListener();
    }

    private void initDialog(View view) {
        this.setCancelable(true);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(view);

        Window window = this.getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.color.colorWhite);
        }
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.setLayout(-1, -2);
            window.setGravity(81);
        }

        rateSeekbar = view.findViewById(R.id.rate_seekbar);
        pitchSeekbar = view.findViewById(R.id.pitch_seekbar);

        ratePercent = view.findViewById(R.id.rate_percent_tv);
        pitchPercent = view.findViewById(R.id.pitch_percent_tv);

        languageCtl = view.findViewById(R.id.language_ctl);
        languageCtl.setOnClickListener(this);
    }

    public void seekbarListener() {
        rateSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ratePercent.setText(progress + "%");
                if (tts != null) {
                    tts.setSpeechRate(convertProgress(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                if (callBack != null) {
                    callBack.onChangeSpeechRate(progress);
                }
            }
        });

        pitchSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                pitchPercent.setText(progress + "%");
                if (tts != null) {
                    tts.setPitch(convertProgress(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (callBack != null) {
                    callBack.onChangePitch(seekBar.getProgress());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.language_ctl:
                if (this.callBack != null) {
                    this.callBack.onShowLanguageDialog();
                }
                break;

        }
    }

    public interface OnEventDocumentActivity {
        void onShowLanguageDialog();

        void onChangePitch(int progress);

        void onChangeSpeechRate(int progress);
    }
}
