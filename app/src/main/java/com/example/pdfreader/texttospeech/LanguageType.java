package com.example.pdfreader.texttospeech;

import android.speech.tts.Voice;

public class LanguageType {

    private String languageTxt;
    private String languageType;
    private boolean isSelected;
    private Voice voice;

    public LanguageType(String languageTxt, String languageType) {
        this.languageTxt = languageTxt;
        this.languageType = languageType;
    }

    public LanguageType(Voice voice) {
        this.voice = voice;
        this.languageTxt = voice.getName();
        this.languageType =  voice.getName();
    }

    public Voice getVoice() {
        return voice;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    public String getLanguageTxt() {
        return languageTxt;
    }

    public void setLanguageTxt(String languageTxt) {
        this.languageTxt = languageTxt;
    }

    public String getLanguageType() {
        return languageType;
    }

    public void setLanguageType(String languageType) {
        this.languageType = languageType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
