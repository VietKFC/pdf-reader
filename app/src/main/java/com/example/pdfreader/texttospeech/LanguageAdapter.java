package com.example.pdfreader.texttospeech;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pdfreader.R;

import java.util.List;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.ViewHolder> {

    private Context context;
    private List<LanguageType> languageTypes;
    private OnClickLanguageItem callBack;

    public LanguageAdapter(Context context) {
        this.context = context;
    }

    public void setCallBack(OnClickLanguageItem callBack) {
        this.callBack = callBack;
    }

    public void setLanguageTypes(List<LanguageType> languageTypes) {
        if (languageTypes == null) {
            return;
        }
        this.languageTypes = languageTypes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public LanguageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.language_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageAdapter.ViewHolder holder, int position) {
        if (this.languageTypes == null) return;

        LanguageType languageType = this.languageTypes.get(position);
        if (languageType != null) {
            holder.languageTv.setText(languageType.getLanguageTxt());
            if (languageType.isSelected()) {
                holder.checkedIv.setVisibility(View.VISIBLE);
            } else {
                holder.checkedIv.setVisibility(View.INVISIBLE);
            }
            holder.constraintLayout.setOnClickListener(v -> {
                if (this.callBack != null) {
                    this.callBack.onClickLanguage(position, languageType.getLanguageType());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.languageTypes == null ? 0 : this.languageTypes.size();
    }

    public interface OnClickLanguageItem {
        void onClickLanguage(int position, String type);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView languageTv;
        ImageView checkedIv;
        ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            languageTv = itemView.findViewById(R.id.tv_language_item);
            checkedIv = itemView.findViewById(R.id.checked_language_item);
            constraintLayout = itemView.findViewById(R.id.ctl_language_item);
        }
    }
}
