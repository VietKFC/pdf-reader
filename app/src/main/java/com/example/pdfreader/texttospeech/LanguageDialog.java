package com.example.pdfreader.texttospeech;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pdfreader.R;
import com.example.pdfreader.utils.AppPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LanguageDialog extends Dialog implements LanguageAdapter.OnClickLanguageItem {

    private RecyclerView languageRcv;
    private List<LanguageType> languageTypeList;
    private LanguageAdapter languageAdapter;
    private OnChooseLanguage callBack;
    private AppPref appPref;
    private ImageView backIv;
    private List<String> listLanguage;
    private List<String> listLanguageDefault;


    public LanguageDialog(@NonNull Context context) {
        super(context);

    }

    public void setCallBack(OnChooseLanguage callBack) {
        this.callBack = callBack;
    }

    public void setListLanguageDefault(List<String> listLanguageDefault) {
        this.listLanguageDefault = listLanguageDefault;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.language_dialog_layout, null, false);
        initDialog(view);
    }

    private void initDialog(View view) {
        this.setCancelable(true);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(view);

        Window window = this.getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.color.colorWhite);
        }
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.setLayout(-1, -2);
            window.setGravity(81);
        }

        this.languageTypeList = new ArrayList<>();
        this.appPref = AppPref.instance(getContext());

        TextView tvTitle = view.findViewById(R.id.tv_language_dl);
        tvTitle.setText("Language");

        this.languageRcv = view.findViewById(R.id.rcv_language_dl);
        this.backIv = view.findViewById(R.id.iv_back_language_dl);

        this.initData();
    }

    private void initData() {
        this.initAdapter();
        initTTSDefaultData();
    }

    private void initAdapter() {
        this.languageAdapter = new LanguageAdapter(getContext());
        this.languageAdapter.setCallBack(this);
        this.languageRcv.setLayoutManager(new LinearLayoutManager(getContext()));
        this.languageRcv.setAdapter(this.languageAdapter);
        this.backIv.setOnClickListener(v -> dismiss());
    }

    private void initTTSDefaultData() {
        if (this.listLanguageDefault == null) {
            return;
        }

        for (String locale : this.listLanguageDefault) {
            if (!TextUtils.isEmpty(locale)) {
                this.languageTypeList.add(new LanguageType(new Locale(locale).getDisplayLanguage(), locale));
            }
        }

        for (int i = 0; i < this.languageTypeList.size(); i++) {
            if (TextUtils.equals(this.languageTypeList.get(i).getLanguageType(), this.appPref.getLanguageTypeDefault())) {
                this.languageTypeList.get(i).setSelected(true);
            }
        }

        this.languageAdapter.setLanguageTypes(this.languageTypeList);
    }


    @Override
    public void onClickLanguage(int position, String type) {
        for (int i = 0; i < this.languageTypeList.size(); i++) {
            if (i == position) {
                this.languageTypeList.get(i).setSelected(true);
            } else {
                this.languageTypeList.get(i).setSelected(false);
            }
        }
        this.languageAdapter.setLanguageTypes(this.languageTypeList);
        if (this.callBack != null) {
            this.callBack.onClickLanguage(this.languageTypeList.get(position).getLanguageType());
        }
        dismiss();
    }

    public interface OnChooseLanguage {
        void onClickLanguage(String type);
    }
}
