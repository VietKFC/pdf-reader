package com.example.pdfreader.texttospeech;

import android.app.Activity;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;

import com.example.pdfreader.utils.AppPref;
import com.pdftron.pdf.ColorPt;
import com.pdftron.pdf.PDFDoc;
import com.pdftron.pdf.PDFViewCtrl;
import com.pdftron.pdf.Page;
import com.pdftron.pdf.Rect;
import com.pdftron.pdf.TextExtractor;
import com.pdftron.pdf.annots.Highlight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class TextToSpeechUtil implements TextToSpeech.OnInitListener {

    private List<String> lines;
    private List<Rect> rects;
    private PDFDoc mPDFDoc;
    private int currentPage;
    private PDFViewCtrl pdfViewCtrl;
    private List<String> languages;
    private TextToSpeech textToSpeech;
    private Activity activity;
    private Highlight currHighlight;
    private Page mPageReading;
    private int currentLine = 0;
    private boolean isReading;

    public TextToSpeechUtil(PDFDoc mPDFDoc , PDFViewCtrl pdfViewCtrl , Activity activity) {
        this.activity = activity;
        this.mPDFDoc = mPDFDoc;
        this.pdfViewCtrl = pdfViewCtrl;
        this.currentPage = pdfViewCtrl.getCurrentPage();
        initTextToSpeech();
    }

    public boolean isReading() {
        return isReading;
    }

    private void initTextToSpeech() {
        this.textToSpeech = new TextToSpeech(activity , this);
    }

    public void getAllLineOfDocument() {
        Page page = null;
        this.lines = new ArrayList<>();
        this.rects = new ArrayList<>();

        try {
            page = mPDFDoc.getPage(this.currentPage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (page == null) {
            return;
        }
        TextExtractor txt = new TextExtractor();
        txt.begin(page);

        TextExtractor.Word word;

        for (TextExtractor.Line line = txt.getFirstLine(); line.isValid(); line = line.getNextLine()) {
            StringBuilder lineBuilder = new StringBuilder();
            for (word = line.getFirstWord(); word.isValid(); word = word.getNextWord()) {
                lineBuilder.append(word.getString());
            }

            this.rects.add(line.getBBox());
            this.lines.add(lineBuilder.toString());
        }

    }

    public void startInitTts() {
        this.textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String s) {

            }

            @Override
            public void onDone(String s) {
                activity.runOnUiThread(() -> {
                    currentLine++;
                    listener.onChangeProgress(currentLine);
                    startSpeak(currentLine);
                });
            }

            @Override
            public void onError(String s) {

            }
        });
    }

    public void onPageChange(boolean b) {
        stopSpeak();
        this.currentPage = pdfViewCtrl.getCurrentPage();
        getAllLineOfDocument();
        listener.onChangePage(lines.size());
        currentLine = 0;
        if(b) {
            startSpeak(currentLine);
        }
    }

    public void stopSpeak() {
        isReading = false;
        removeHighLight();
        this.currentLine = 0;
        this.textToSpeech.stop();
    }

    public void pauseSpeak() {
        isReading = false;
        this.textToSpeech.stop();
    }

    public void resumeSpeak() {
        isReading = true;
        startSpeak(currentLine);
    }

    public void nextLineReader() {
        removeHighLight();
        this.textToSpeech.stop();
        if(currentLine < lines.size() - 1) {
            listener.onChangeProgress(currentLine);
            currentLine++;
        }
        resumeSpeak();
    }

    public void backLineReader() {
        removeHighLight();
        this.textToSpeech.stop();
        if(currentLine > 1) {
            currentLine--;
        }
        resumeSpeak();
    }

    private void removeHighLight() {
        try {
            PDFDoc doc = pdfViewCtrl.getDoc();
            if (doc == null) {
                return;
            }
            doc.initSecurityHandler();

            Page page;

            if (currHighlight != null) {
                page = doc.getPage(currentPage);
                page.annotRemove(currHighlight);
                pdfViewCtrl.update(currHighlight, currentPage);
                pdfViewCtrl.clearSelection();
                currHighlight = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAudioSettingDialog() {
        AudioDialog audioDialog = new AudioDialog(activity, textToSpeech);
        audioDialog.setCallBack(new AudioDialog.OnEventDocumentActivity() {
            @Override
            public void onShowLanguageDialog() {
                LanguageDialog languageDialog = new LanguageDialog(activity);
                languageDialog.setListLanguageDefault(languages);
                languageDialog.setCallBack(type -> {
                    AppPref.instance(activity).setLanguageTypeDefault(type);
                    textToSpeech.stop();
                    textToSpeech.setLanguage(new Locale(type));
                    startSpeak(currentLine);
                });
                languageDialog.show();
            }

            @Override
            public void onChangePitch(int progress) {
                AppPref.instance(activity).setPitch(progress);
                textToSpeech.setPitch(AudioDialog.convertProgress(progress));
            }

            @Override
            public void onChangeSpeechRate(int progress) {
                AppPref.instance(activity).setSpeed(progress);
                textToSpeech.setSpeechRate(AudioDialog.convertProgress(progress));
            }
        });
        audioDialog.show();
    }

    public void startSpeak(int position) {
        isReading = true;
        HashMap<String , String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID , "UniqueId");
        if(rects.size() <= 0 || lines.size() <= 0) {
            return;
        }
        setHighLight(rects.get(position));
        this.textToSpeech.speak(this.lines.get(position) , TextToSpeech.QUEUE_FLUSH , map);
    }

    private void setHighLight(Rect rect) {
        if (activity != null) {
            activity.runOnUiThread(() -> {
                try {
                    PDFDoc doc = pdfViewCtrl.getDoc();
                    if (doc == null) {
                        return;
                    }
                    doc.initSecurityHandler();

                    if (currHighlight != null && mPageReading != null) {
                        mPageReading.annotRemove(currHighlight);
                        pdfViewCtrl.update(currHighlight, currentPage);
                        pdfViewCtrl.clearSelection();
                        currHighlight = null;
                    }

                    currentPage = pdfViewCtrl.getCurrentPage();
                    mPageReading = doc.getPage(currentPage);
                    currHighlight = Highlight.create(doc, rect);
                    currHighlight.setColor(new ColorPt(1, 1, 0), 3);
                    currHighlight.refreshAppearance();

                    mPageReading.annotPushBack(currHighlight);

                    pdfViewCtrl.update(currHighlight, currentPage);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onInit(int i) {
        this.getAllLineOfDocument();
        this.startInitTts();
        this.languages = new ArrayList<>();

        Locale[] availableLocales = Locale.getAvailableLocales();

        for (Locale availableLocale : availableLocales) {
            int res = this.textToSpeech.isLanguageAvailable(availableLocale);

            if (res == TextToSpeech.LANG_COUNTRY_AVAILABLE && !languages.contains(availableLocale.getLanguage())) {
                languages.add(availableLocale.getLanguage());
            }

        }

        this.textToSpeech.setSpeechRate(AudioDialog.convertProgress(AppPref.instance(activity).getSpeed()));
        this.textToSpeech.setPitch(AudioDialog.convertProgress(AppPref.instance(activity).getPitch()));

    }

    public interface OnChangeSpeakProgressListener{
        void onChangeProgress(int position);
        void onChangePage(int totalLine);
    }

    private OnChangeSpeakProgressListener listener;

    public void setListener(OnChangeSpeakProgressListener listener) {
        this.listener = listener;
    }
}
