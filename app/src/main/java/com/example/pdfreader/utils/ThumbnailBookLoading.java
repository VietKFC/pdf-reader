package com.example.pdfreader.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Pair;

import com.pdftron.pdf.PDFDoc;
import com.pdftron.pdf.PDFDraw;
import com.pdftron.pdf.Page;

import io.reactivex.Single;

public class ThumbnailBookLoading {

    private Context mContext;
    private String path;

    public ThumbnailBookLoading(Context mContext, String modelPath) {
        this.mContext = mContext;
        this.path = modelPath;
    }

    public Single<Pair<String, Boolean>> excute() {
        return Single.create(emitter -> {
            Bitmap bitmap = null;
            boolean isLocked = false;
            try {
                PDFDraw pdfDraw = new PDFDraw();
                PDFDoc pdfDoc = new PDFDoc(path);
                isLocked = !pdfDoc.initSecurityHandler();
                if (!isLocked) {
                    pdfDraw.setDPI(32);
                    Page page = pdfDoc.getPage(1);
                    bitmap = pdfDraw.getBitmap(page);
                    pdfDoc.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            String pathCache = "";
            if (bitmap != null) {
                pathCache = FileUtils.addThumbCache(mContext, path, bitmap);
            }

            if (bitmap != null && bitmap.isRecycled()) {
                bitmap.recycle();
            }
            emitter.onSuccess(new Pair<>(pathCache, isLocked));
        });
    }
}
