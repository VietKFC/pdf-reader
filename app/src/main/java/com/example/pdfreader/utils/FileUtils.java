package com.example.pdfreader.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.example.pdfreader.BuildConfig;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;

public class FileUtils {


    private final static String THUMBNAIL_TEMP_FOLDER = "ThumbCache";


    public static String convertBytes(long size) {
        long Kb = 1024;
        long Mb = Kb * 1024;
        long Gb = Mb * 1024;
        long Tb = Gb * 1024;
        long Pb = Tb * 1024;
        long Eb = Pb * 1024;

        if (size < Kb) return floatForm(size) + "KB ";
        if (size < Mb) return floatForm((double) size / Kb) + "KB ";
        if (size < Gb) return floatForm((double) size / Mb) + "MB ";
        if (size < Tb) return floatForm((double) size / Gb) + "GB ";
        if (size < Pb) return floatForm((double) size / Tb) + "TB ";
        if (size < Eb) return floatForm((double) size / Pb) + "PB ";
        return floatForm((double) size / Eb) + "EB ";

    }

    public static String floatForm(double d) {
        return new DecimalFormat("#.##").format(d);
    }

    public static String getPathThumbCache(@NonNull Context context, String path) {
        String filePath = path.hashCode() + ".jpg";
        return getFolderThumbCacheParent(context) + File.separator + filePath;
    }

    private static String getFolderThumbCacheParent(Context context) {
        String filepath = context.getFilesDir().getPath();

        File file = new File(filepath, THUMBNAIL_TEMP_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }

    public static boolean bitmapToFile(String path, Bitmap bitmap) {
        File file;
        try {
            file = new File(path);
            if (file.exists()) {
                return true;
            }
            file.createNewFile();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String addThumbCache(@NonNull Context context, String path, Bitmap image) {
        if (image == null || TextUtils.isEmpty(path)) {
            return "";
        }

        String filePath = getPathThumbCache(context, path);

        if (bitmapToFile(filePath, image)) {
            return filePath;
        }
        return "";
    }

    public static boolean isAllBackSpace(String input) {
        int count = 0;
        for (char character : input.toCharArray()) {
            if (!TextUtils.equals(String.valueOf(character), " ")) {
                count++;
            }
        }
        if (count > 0) {
            return false;
        }
        return true;
    }

    public static String renameFile(Context context, String newName, String filePath) {
        File from = new File(filePath);
        if (!from.exists()) {
            Toast.makeText(context, "This name has already exist!", Toast.LENGTH_SHORT).show();
            return "";
        }
        String substring = from.getParent();
        File to = new File(substring, newName);
        if (!to.exists()) {
            if (from.renameTo(to)) {
                deleteFile(filePath);
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(to)));
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(from)));
                return to.getPath();
            }
        }
        Toast.makeText(context, "This name has already exist!", Toast.LENGTH_SHORT).show();
        return "";
    }

    public static boolean deleteFile(String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void shareFile(String filePath, Context context) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri screenshotUri = createUri(context, filePath);
        sharingIntent.setType("*/*");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
        context.startActivity(Intent.createChooser(sharingIntent, "Share this file using"));
    }

    public static Uri createUri(Context context, String str) {
        if (Build.VERSION.SDK_INT < 24) {
            return Uri.fromFile(new File(str));
        }
        try {
            String provideName = BuildConfig.APPLICATION_ID + ".provider";
            return FileProvider.getUriForFile(context, provideName, new File(str));
        } catch (IllegalArgumentException e) {
            return new Uri.Builder().build();
        }
    }

    public static void copy(File src, File dst, Context context) throws IOException {
        if (!dst.exists()) {
            try {
                dst.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(dst));
        context.sendBroadcast(intent);

    }

}
