package com.example.pdfreader.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.play.core.install.model.AppUpdateType;

import java.lang.ref.WeakReference;

public class AppPref {

    private static AppPref appPref;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public AppPref(Context context){
        this.sharedPreferences = context.getSharedPreferences("pdf_view_pref" , Context.MODE_PRIVATE);
        this.editor = this.sharedPreferences.edit();
    }

    public static AppPref instance(Context context) {
        WeakReference<Context> contextWeakReference = new WeakReference<>(context);
        if (appPref == null) {
            appPref = new AppPref(contextWeakReference.get());
        }
        return appPref;
    }

    public void setSingleMode(boolean isSingle){
        this.editor.putBoolean("single_mode_pref" , isSingle).apply();
    }

    public boolean isDoubleMode(){
        return this.sharedPreferences.getBoolean("single_mode_pref" , false);
    }

    public void setVerticalMode(boolean isVertical){
        this.editor.putBoolean("vertical_mode_pref" , isVertical).apply();
    }

    public boolean isVerticalMode(){
        return this.sharedPreferences.getBoolean("vertical_mode_pref" , false);
    }

    public void setSpeed(int progress){
        this.editor.putInt("speed_progress_pref" , progress).apply();
    }

    public int getSpeed(){
        return this.sharedPreferences.getInt("speed_progress_pref" , 50);
    }

    public void setPitch(int progress){
        this.editor.putInt("pitch_progress_pref" , progress).apply();
    }

    public int getPitch(){
        return this.sharedPreferences.getInt("pitch_progress_pref" , 50);
    }

    public void setLanguageTypeDefault(String type) {
        this.editor.putString("language_tts_pref", type).apply();
    }

    public boolean isSkipOnBoarding() {
        return this.sharedPreferences.getBoolean("key_skip_onboarding", false);
    }

    public void setSkipOnBoarding(boolean status) {
        this.editor.putBoolean("key_skip_onboarding", status).apply();
    }

    public String getLanguageTypeDefault() {
        return this.sharedPreferences.getString("language_tts_pref" , "");
    }

    public int getFileTypeOptionPosition() {
        return this.sharedPreferences.getInt("KEY_FILE_TYPE_OPTION", 5);
    }

    public void setFileTypeOptionPosition(int type) {
        this.editor.putInt("KEY_FILE_TYPE_OPTION", type).apply();
    }

    public int getInAppUpdateType() {
        return this.sharedPreferences.getInt("key_in_app_update", AppUpdateType.IMMEDIATE);
    }

    public void setInAppUpdateType(int type) {
        this.editor.putInt("key_in_app_update", type).apply();
    }
}
