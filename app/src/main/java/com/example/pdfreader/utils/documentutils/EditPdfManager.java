package com.example.pdfreader.utils.documentutils;

import android.view.MenuItem;
import android.view.View;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.example.pdfreader.R;
import com.example.pdfreader.databinding.ActivityDocumentReaderBinding;
import com.pdftron.common.PDFNetException;
import com.pdftron.pdf.PDFDoc;
import com.pdftron.pdf.PDFViewCtrl;
import com.pdftron.pdf.Page;
import com.pdftron.pdf.controls.FindTextOverlay;
import com.pdftron.pdf.controls.SearchToolbar;
import com.pdftron.pdf.controls.UserCropDialogFragment;
import com.pdftron.pdf.dialog.RotateDialogFragment;
import com.pdftron.pdf.tools.AdvancedShapeCreate;
import com.pdftron.pdf.tools.ToolManager;

import static android.view.View.VISIBLE;

public class EditPdfManager {

    private PDFViewCtrl pdfViewCtrl;
    private ActivityDocumentReaderBinding binding;
    private PDFDoc pdfDoc;

    public EditPdfManager(PDFViewCtrl pdfViewCtrl, ActivityDocumentReaderBinding binding, PDFDoc pdfDoc) {
        this.pdfViewCtrl = pdfViewCtrl;
        this.binding = binding;
        this.pdfDoc = pdfDoc;
    }

    public void initSearchText(SearchToolbar searchToolbar , FindTextOverlay findTextOverlay){
        findTextOverlay.setPdfViewCtrl(pdfViewCtrl);
        searchToolbar.setSearchToolbarListener(new SearchToolbar.SearchToolbarListener() {
            @Override
            public void onExitSearch() {
                searchToolbar.setVisibility(View.GONE);
                findTextOverlay.setVisibility(View.GONE);
                findTextOverlay.exitSearchMode();
            }

            @Override
            public void onClearSearchQuery() {
                findTextOverlay.cancelFindText();
            }

            @Override
            public void onSearchQuerySubmit(String s) {
                findTextOverlay.queryTextSubmit(s);
            }

            @Override
            public void onSearchQueryChange(String s) {
                findTextOverlay.setSearchQuery(s);
            }

            @Override
            public void onSearchOptionsItemSelected(MenuItem menuItem, String s) {
                int id = menuItem.getItemId();
                if (id == R.id.action_match_case) {
                    boolean isChecked = menuItem.isChecked();
                    findTextOverlay.setSearchMatchCase(!isChecked);
                    findTextOverlay.resetFullTextResults();
                    menuItem.setChecked(!isChecked);
                } else if (id == R.id.action_whole_word) {
                    boolean isChecked = menuItem.isChecked();
                    findTextOverlay.setSearchWholeWord(!isChecked);
                    findTextOverlay.resetFullTextResults();
                    menuItem.setChecked(!isChecked);
                }
            }
        });
        searchToolbar.setVisibility(VISIBLE);
        findTextOverlay.setVisibility(VISIBLE);
    }

    public void showCropDialog(FragmentManager fragmentManager){
        UserCropDialogFragment fragment = UserCropDialogFragment.newInstance();
        fragment.setPdfViewCtrl(pdfViewCtrl);
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomAppTheme);
        try {
            pdfViewCtrl.setPageBox(Page.e_user_crop);
        } catch (PDFNetException e) {
            e.printStackTrace();
        }
        fragment.show(fragmentManager, "user_crop_pages_dialog");
    }

    public void showRotateDialog(FragmentManager fragmentManager){
        RotateDialogFragment rotateDialogFragment = RotateDialogFragment.newInstance();
        rotateDialogFragment
                .setPdfViewCtrl(this.pdfViewCtrl)
                .show(fragmentManager , "rotate_pages_dialog");
    }

    public void resetEditTool(ToolManager toolManager) {
        ToolUtils.setToolMode(ToolManager.ToolMode.PAN, toolManager);
    }

    public void commitAdvanceShapeTool(ToolManager mToolManager) {
        if (mToolManager.getTool() instanceof AdvancedShapeCreate) {
            AdvancedShapeCreate tool = (AdvancedShapeCreate) mToolManager.getTool();
            tool.commit();
        }
    }

    public void saveIncrementally() {
        try {
            this.binding.pdfViewCtrl.docLock(true, () -> binding.pdfViewCtrl.getDoc().save()
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
