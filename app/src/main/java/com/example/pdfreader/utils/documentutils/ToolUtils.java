package com.example.pdfreader.utils.documentutils;

import android.content.Context;
import android.graphics.Color;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.example.pdfreader.Constants;
import com.example.pdfreader.R;
import com.example.pdfreader.model.AnnotModel;
import com.pdftron.pdf.PDFViewCtrl;
import com.pdftron.pdf.controls.ThumbnailSlider;
import com.pdftron.pdf.controls.ThumbnailsViewFragment;
import com.pdftron.pdf.tools.Tool;
import com.pdftron.pdf.tools.ToolManager;

import java.util.List;

import static android.graphics.Color.HSVToColor;
import static android.graphics.Color.RGBToHSV;

public class ToolUtils {


    public static int getViewerBackgroundColor(int color) {

        float[] hsv = new float[3];
        RGBToHSV(Color.red(color), Color.green(color), Color.blue(color), hsv);
        float hue = hsv[0] / 360f;
        float saturation = hsv[1];
        float value = hsv[2];

        float lowEarthHue = 0.05f;
        float highEarthHue = 0.11f;
        boolean earthTones = hue >= lowEarthHue && hue <= highEarthHue;
        if (value > 0.5) {
            if (earthTones) {
                value -= 0.2;
                saturation = Math.min(saturation * 2, Math.min(saturation + 0.05f, 1.0f));
            } else {
                value *= 0.6;
            }
        } else if (value >= 0.3) {
            value = (value / 2) + 0.05f;
        } else if (value >= 0.1) {
            value -= 0.1;
        } else {
            value += 0.1;
        }
        if (!earthTones) {
            float dist = Math.min(0.05f, lowEarthHue - hue);
            if (hue > highEarthHue) {
                dist = Math.min(0.05f, hue - highEarthHue);
            }
            saturation = saturation - (saturation * (20f * dist) * 0.6f);
        }

        hsv[0] = hue * 360f;
        hsv[1] = saturation;
        hsv[2] = value;
        return HSVToColor(hsv);
    }

    public static void initThumbnailSlider(ThumbnailSlider thumbnailSlider, PDFViewCtrl pdfViewCtrl, Context context, FragmentManager fragmentManager) {
        thumbnailSlider.setPdfViewCtrl(pdfViewCtrl);
        thumbnailSlider.setOnMenuItemClickedListener(new ThumbnailSlider.OnMenuItemClickedListener() {
            @Override
            public void onMenuItemClicked(int menuItemPosition) {
                if (menuItemPosition == ThumbnailSlider.POSITION_LEFT) {
                    ThumbnailsViewFragment mThumbFragment = ThumbnailsViewFragment.newInstance(false, true);
                    mThumbFragment.setPdfViewCtrl(pdfViewCtrl);
                    mThumbFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomAppTheme);
                    mThumbFragment.setTitle(context.getString(com.pdftron.pdf.tools.R.string.pref_viewmode_thumbnails_title));


                    mThumbFragment.show(fragmentManager, "thumbnails_fragment");
                    return;
                }
            }
        });


        pdfViewCtrl.addPageChangeListener((i, i1, pageChangeState) -> {
                    thumbnailSlider.setProgress(pdfViewCtrl.getCurrentPage());
                }
        );
    }

    public static void initAnnotationData(List<AnnotModel> annotModelList , Context context) {
        annotModelList.add(new AnnotModel(context.getString(R.string.text_brush), R.drawable.ic_brush , Constants.BRUSH_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_signature), R.drawable.ic_signature , Constants.SIGNATURE));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_note), R.drawable.ic_note , Constants.NOTE_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_text), R.drawable.ic_text , Constants.TEXT_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_image), R.drawable.ic_image , Constants.IMAGE_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_multi_select), R.drawable.ic_multi_select , Constants.MULTI_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_free_highlight), R.drawable.ic_free_highlight , Constants.FREEHL_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_rectangle), R.drawable.ic_rectangle , Constants.RECTANGLE_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_line), R.drawable.edit_vector_line , Constants.LINE_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_stamp), R.drawable.ic_stamp , Constants.STAMP_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_highlight), R.drawable.ic_highlight , Constants.HIGHLIGHT_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_underline), R.drawable.ic_underline , Constants.UNDERLINE_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_squiggly), R.drawable.ic_squiggly , Constants.SQUIGGLY_TOOL));
        annotModelList.add(new AnnotModel(context.getString(R.string.text_strikeout), R.drawable.ic_strikeout , Constants.STRIKEOUT_TOOL));
    }

    public static void setToolMode(ToolManager.ToolModeBase toolMode , ToolManager toolManager) {
        if (toolMode == ToolManager.ToolMode.ANNOT_EDIT_RECT_GROUP) {
            toolManager.setTool(toolManager.createTool(ToolManager.ToolMode.ANNOT_EDIT_RECT_GROUP, null));
        } else {
            toolManager.setTool(toolManager.createTool(toolMode, toolManager.getTool()));
        }
        ((Tool) toolManager.getTool()).setForceSameNextToolMode(true);
    }

    public static void setToolType(ToolManager toolManager , int toolType) {
        switch (toolType) {
            case Constants.NOTE_TOOL:
                setToolMode(ToolManager.ToolMode.TEXT_ANNOT_CREATE, toolManager);
                break;
            case Constants.BRUSH_TOOL:
                setToolMode(ToolManager.ToolMode.INK_CREATE, toolManager);
                break;
            case Constants.SIGNATURE:
                setToolMode(ToolManager.ToolMode.SIGNATURE, toolManager);
                break;
            case Constants.LINE_TOOL:
                setToolMode(ToolManager.ToolMode.LINE_CREATE, toolManager);
                break;
            case Constants.PAN_TOOL:
                setToolMode(ToolManager.ToolMode.PAN, toolManager);
                break;
            case Constants.TEXT_TOOL:
                setToolMode(ToolManager.ToolMode.TEXT_CREATE, toolManager);
                break;
            case Constants.ERASER_TOOL:
                setToolMode(ToolManager.ToolMode.INK_ERASER, toolManager);
                break;
            case Constants.IMAGE_TOOL:
                setToolMode(ToolManager.ToolMode.STAMPER, toolManager);
                break;
            case Constants.MULTI_TOOL:
                setToolMode(ToolManager.ToolMode.ANNOT_EDIT_RECT_GROUP , toolManager);
                break;
            case Constants.FREEHL_TOOL:
                setToolMode(ToolManager.ToolMode.FREE_HIGHLIGHTER, toolManager);
                break;
            case Constants.RECTANGLE_TOOL:
                setToolMode(ToolManager.ToolMode.RECT_CREATE, toolManager);
                break;
            case Constants.STAMP_TOOL:
                setToolMode(ToolManager.ToolMode.RUBBER_STAMPER, toolManager);
                break;
            case Constants.HIGHLIGHT_TOOL:
                setToolMode(ToolManager.ToolMode.TEXT_HIGHLIGHT, toolManager);
                break;
            case Constants.UNDERLINE_TOOL:
                setToolMode(ToolManager.ToolMode.TEXT_UNDERLINE, toolManager);
                break;
            case Constants.SQUIGGLY_TOOL:
                setToolMode(ToolManager.ToolMode.TEXT_SQUIGGLY, toolManager);
                break;
            case Constants.STRIKEOUT_TOOL:
                setToolMode(ToolManager.ToolMode.TEXT_STRIKEOUT, toolManager);
                break;
        }
    }

    public static void undoRedoAction(PDFViewCtrl pdfViewCtrl, boolean isUndo) {
        String info = null;
        try {
            pdfViewCtrl.cancelRendering();
            if (isUndo) {
                info = pdfViewCtrl.undo();
            } else {
                info = pdfViewCtrl.redo();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
