package com.example.pdfreader.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;

import com.example.pdfreader.model.FileModel;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class ExternalDataLoading {

    public static Observable<List<FileModel>> getData(Context context){
        return Observable.create(emitter -> {
            loadAllDocument(context  , emitter);

            emitter.onComplete();
        });
    }

    private static void loadAllDocument(Context context , ObservableEmitter<List<FileModel>> emitter) {
        MimeTypeMap singleTon = MimeTypeMap.getSingleton();

        String pdf = singleTon.getMimeTypeFromExtension("pdf");
        String txt = singleTon.getMimeTypeFromExtension("txt");
        String doc = singleTon.getMimeTypeFromExtension("doc");
        String docx = singleTon.getMimeTypeFromExtension("docx");
        String ppt = singleTon.getMimeTypeFromExtension("ppt");
        String xlsx = singleTon.getMimeTypeFromExtension("xlsx");

        Uri uri = MediaStore.Files.getContentUri("external");

        String[] columns = {"_id",
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.DATE_MODIFIED,
                MediaStore.Files.FileColumns.TITLE,
                MediaStore.Files.FileColumns.MIME_TYPE
        };

        String where =
                MediaStore.Files.FileColumns.MIME_TYPE + "=?"
                        + " OR " + MediaStore.Files.FileColumns.MIME_TYPE + "=?"
                        + " OR " + MediaStore.Files.FileColumns.MIME_TYPE + "=?"
                        + " OR " + MediaStore.Files.FileColumns.MIME_TYPE + "=?"
                        + " OR " + MediaStore.Files.FileColumns.MIME_TYPE + "=?"
                        + " OR " + MediaStore.Files.FileColumns.MIME_TYPE + "=?";

        String[] args = new String[]{pdf , txt , doc , docx , ppt , xlsx};
        Cursor cursor = context.getContentResolver().query(uri , columns , where , args , "date_modified DESC");
        List<FileModel> fileModelList1 = new ArrayList<>();
        if(cursor != null && cursor.getCount() > 0){
            while (cursor.moveToNext()){
                if(emitter.isDisposed()){
                    return;
                }

                try{
                    String path = cursor.getString(1);
                    File file = new File(path);
                    String extension = path.substring(path.lastIndexOf(".") + 1);
                    double fileSize =  Double.parseDouble(String.valueOf(file.length() / 1024));
                    fileModelList1.add(new FileModel(path , fileSize , new Date(file.lastModified()), extension.toUpperCase()));
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
            if(!fileModelList1.isEmpty()){
                emitter.onNext(fileModelList1);
            }

            cursor.close();

        }

    }
}
