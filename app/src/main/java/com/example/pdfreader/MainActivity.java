package com.example.pdfreader;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.pdfreader.adapter.FileListAdapter;
import com.example.pdfreader.adapter.PopupMenuOptionAdapter;
import com.example.pdfreader.databinding.ActivityMainBinding;
import com.example.pdfreader.filemenu.OptionPdfFileDialog;
import com.example.pdfreader.model.DocumentOptionModel;
import com.example.pdfreader.model.FileModel;
import com.example.pdfreader.setting.SettingActivity;
import com.example.pdfreader.utils.AppPref;
import com.example.pdfreader.utils.ExternalDataLoading;
import com.ferfalk.simplesearchview.SimpleSearchView;
import com.pdftron.pdf.utils.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener,
        PopupMenuOptionAdapter.OnClickPopupMenuOption {

    private ActivityMainBinding binding;
    private List<FileModel> fileModelList;
    private FileListAdapter fileListAdapter;
    private PopupMenuOptionAdapter popupMenuOptionAdapter;
    private List<DocumentOptionModel> documentOptionModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(this.binding.getRoot());
        initView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        } else {
            initData();
        }
    }

    private void initData() {
        this.binding.prbExternalLoading.setVisibility(View.VISIBLE);
        ExternalDataLoading.getData(this)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<FileModel>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<FileModel> fileModels) {
                        fileModelList.addAll(fileModels);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        binding.prbExternalLoading.setVisibility(View.GONE);
                        fileListAdapter.setFileModelList(filterFileTypeList());
                        binding.rvFile.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                        binding.rvFile.setAdapter(fileListAdapter);
                    }
                });
    }

    public void findData(String key) {
        if (this.fileModelList == null) {
            this.fileModelList = new ArrayList<>();
        }

        List<FileModel> fileModelListChild = new ArrayList<>();
        for (int i = 0; i < this.fileModelList.size(); i++) {
            if (new File(this.fileModelList.get(i).getFilePath()).getName().contains(key)) {
                fileModelListChild.add(this.fileModelList.get(i));
            }
        }

        this.fileListAdapter.setFileModelList(fileModelListChild);
    }

    private void initView() {
        this.fileListAdapter = new FileListAdapter(this);
        this.popupMenuOptionAdapter = new PopupMenuOptionAdapter(this);
        this.popupMenuOptionAdapter.setPopupMenuCallBack(this);
        binding.ivMenuMain.setOnClickListener(this);
        this.binding.ctlMenuContainer.setOnClickListener(this);
        this.fileListAdapter.setCallback(new FileListAdapter.OnEventToActivity() {
            @Override
            public void startDocumentActivity(String path) {
                Intent intent = new Intent(MainActivity.this, DocumentReaderActivity.class);
                intent.putExtra("pdf_path_key", path);
                MainActivity.this.startActivity(intent);
            }

            @Override
            public void onClickMenu(FileModel fileModel) {
                OptionPdfFileDialog optionPdfFileDialog = new OptionPdfFileDialog(MainActivity.this,
                        fileModel,
                        fileModelList.indexOf(fileModel));
                optionPdfFileDialog.setCallBack(new OptionPdfFileDialog.OnEventBrowseFileFragment() {
                    @Override
                    public void onDeleteFile(int position) {
                        fileModelList.remove(position);
                        fileListAdapter.setFileModelList(filterFileTypeList());
                    }

                    @Override
                    public void onRenameFile(String newName, int position) {
                       fileModelList.get(position).setFilePath(fileModelList.get(position).getFilePath().
                               replace(fileModelList.get(position).getFilePath().
                                       substring(fileModelList.get(position).getFilePath().lastIndexOf("/") + 1) ,
                                       newName));
                       fileListAdapter.setFileModelList(filterFileTypeList());
                    }

                    @Override
                    public void onDuplicateFile() {
                        onClickEventRefreshData();
                    }
                });
                optionPdfFileDialog.show();
            }
        });
        this.fileModelList = new ArrayList<>();

        this.binding.ivMainSearch.setOnClickListener(this);
        this.binding.searchViewMain.setOnQueryTextListener(new SimpleSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                findData(query);
                hideKeyboard();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                findData(newText);
                return false;
            }

            @Override
            public boolean onQueryTextCleared() {
                findData("");
                return false;
            }
        });

        this.binding.searchViewMain.setOnSearchViewListener(new SimpleSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {
                findData("");
                hideKeyboard();
            }

            @Override
            public void onSearchViewShownAnimation() {

            }

            @Override
            public void onSearchViewClosedAnimation() {

            }
        });
        documentOptionModelList = new ArrayList<>();
        documentOptionModelList.add(new DocumentOptionModel(R.drawable.ic_scan, 0, "Refresh"));
        documentOptionModelList.add(new DocumentOptionModel(R.drawable.ic_home_setting_black, 0, "Setting"));
        documentOptionModelList.add(new DocumentOptionModel(0, 0, null));
        documentOptionModelList.add(new DocumentOptionModel(R.drawable.ic_red_checked, 0, null));
        documentOptionModelList.add(new DocumentOptionModel(R.drawable.ic_red_checked, 0, "All"));
        documentOptionModelList.add(new DocumentOptionModel(R.drawable.ic_red_checked, 0, "PDF"));
        documentOptionModelList.add(new DocumentOptionModel(R.drawable.ic_red_checked, 0, "Doc"));
        documentOptionModelList.add(new DocumentOptionModel(R.drawable.ic_red_checked, 0, "Image"));
        this.popupMenuOptionAdapter.setDocumentOptionModels(documentOptionModelList);
        this.binding.rcvOptionMain.setAdapter(this.popupMenuOptionAdapter);
    }

    public void hideKeyboard() {
        InputMethodManager imm = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        }
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @androidx.annotation.NonNull String[] permissions, @androidx.annotation.NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initData();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_main_search:
                binding.searchViewMain.setVisibility(View.VISIBLE);
                binding.searchViewMain.showSearch();
                break;

            case R.id.iv_menu_main:
                binding.ctlMenuContainer.setVisibility(View.VISIBLE);
                break;

            case R.id.ctl_menu_container:
                binding.ctlMenuContainer.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyboard();
    }

    public List<FileModel> filterFileTypeList() {
        switch (AppPref.instance(this).getFileTypeOptionPosition()) {
            case 4:
                return this.fileModelList;

            case 5:
                List<FileModel> listDataFilter = new ArrayList<>();
                for (FileModel model : this.fileModelList) {
                    if (model.getMimeType() != null && TextUtils.equals(model.getMimeType().toUpperCase(), "PDF")) {
                        listDataFilter.add(model);
                    }
                }
                return listDataFilter;
            case 6:
                Log.e("viet", "filterFileTypeList: " );
                List<FileModel> lisDataFilterDoc = new ArrayList<>();
                List<String> docType = new ArrayList<>(Arrays.asList(Constants.FILE_NAME_EXTENSIONS_DOC));

                for (FileModel model : this.fileModelList) {
                    if (docType.contains(model.getMimeType().toLowerCase()) && model.getMimeType() != null) {
                        lisDataFilterDoc.add(model);
                    }

                }
                return lisDataFilterDoc;
            case 7:
                List<FileModel> listDataFilterImg = new ArrayList<>();
                List<String> imageType = new ArrayList<>(Arrays.asList(Constants.FILE_NAME_EXTENSIONS_IMAGE));

                for (FileModel model : this.fileModelList) {
                    if (model.getMimeType() != null && imageType.contains(model.getMimeType().toLowerCase())) {
                        listDataFilterImg.add(model);
                    }
                }
                return listDataFilterImg;
        }
        return new ArrayList<>();
    }

    @Override
    public void onClickMenuItemFileType(int type) {
        AppPref.instance(this).setFileTypeOptionPosition(type);
        this.popupMenuOptionAdapter.notifyDataSetChanged();
        this.fileListAdapter.setFileModelList(filterFileTypeList());
    }

    @Override
    public void onClickEventRefreshData() {
        this.fileModelList.clear();
        this.initData();
        AppPref.instance(this).setFileTypeOptionPosition(4);
        fileListAdapter.notifyDataSetChanged();
        popupMenuOptionAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickEventSettingFragment() {
        startActivity(new Intent(MainActivity.this , SettingActivity.class));
    }

    @Override
    public void onClosePopupMenu() {

    }

}