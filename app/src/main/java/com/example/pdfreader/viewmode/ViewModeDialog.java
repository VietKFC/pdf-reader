package com.example.pdfreader.viewmode;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.example.pdfreader.DocumentReaderActivity;
import com.example.pdfreader.R;
import com.example.pdfreader.databinding.ViewmodeDialogBinding;
import com.example.pdfreader.utils.AppPref;
import com.pdftron.pdf.dialog.CustomColorModeDialogFragment;
import com.pdftron.pdf.utils.PdfViewCtrlSettingsManager;

public class ViewModeDialog implements View.OnClickListener{

    private ViewmodeDialogBinding binding;
    private Dialog dialog;
    private View view;
    private Context context;
    private AppPref appPref;

    public ViewModeDialog( Context context) {
        this.context = context;
        this.appPref = AppPref.instance(context);
        this.view = getView();
    }

    private View getView() {
        this.dialog = new Dialog(context);
        this.binding = ViewmodeDialogBinding.inflate(LayoutInflater.from(context));
        this.binding.clLightMode.setOnClickListener(this);
        this.binding.clDarkMode.setOnClickListener(this);
        this.binding.clSepiaMode.setOnClickListener(this);
        this.binding.clMore.setOnClickListener(this);
        this.binding.tvViewModeSelection.setOnClickListener(this);
        this.binding.clVerticalScroll.setOnClickListener(this);
        this.binding.clReadOnly.setOnClickListener(this);
        this.binding.clCrop.setOnClickListener(this);
        this.binding.clRotate.setOnClickListener(this);

        initViewColor();
        initReadingMode();
        return binding.getRoot();

    }

    private void initReadingMode() {
        this.binding.tvViewModeSelection.setText(this.appPref.isDoubleMode()? "Double": "Single");
        this.binding.ivViewModeSelection2.setVisibility(this.appPref.isDoubleMode()?View.VISIBLE:View.GONE);
        this.binding.swVerticalScroll.setChecked(this.appPref.isVerticalMode());
    }

    private void initViewColor() {
        switch (PdfViewCtrlSettingsManager.getColorMode(context)){
            case PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_NORMAL:
                setCheckedViewColor(View.VISIBLE , View.INVISIBLE , View.INVISIBLE, View.INVISIBLE);
                break;
            case PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_NIGHT:
                setCheckedViewColor(View.INVISIBLE , View.VISIBLE , View.INVISIBLE, View.INVISIBLE);
                break;
            case PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_SEPIA:
                setCheckedViewColor(View.INVISIBLE , View.INVISIBLE , View.VISIBLE, View.INVISIBLE);
                break;
            case PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_CUSTOM:
                setCheckedViewColor(View.INVISIBLE , View.INVISIBLE , View.INVISIBLE, View.VISIBLE);
                break;
        }
    }

    public void show(){
        ((Activity) context).runOnUiThread(() -> {
            Window window = dialog.getWindow();
            if (window != null) {
                window.setBackgroundDrawableResource(R.color.colorWhite);
            }

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(view);
            dialog.setCancelable(true);


            if (window != null) {
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                window.setLayout(-1, -2);
                window.setGravity(81);
            }
            if (!((Activity) context).isFinishing()) {
                dialog.show();
                dialog.show();
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.cl_light_mode:
                setCheckedViewColor(View.VISIBLE , View.INVISIBLE , View.INVISIBLE, View.INVISIBLE);
                if(callback != null){
                    callback.onChangeViewColor(PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_NORMAL);
                }
                break;
            case R.id.cl_dark_mode:
                setCheckedViewColor(View.INVISIBLE , View.VISIBLE , View.INVISIBLE, View.INVISIBLE);
                if(callback != null){
                    callback.onChangeViewColor(PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_NIGHT);
                }
                break;
            case R.id.cl_sepia_mode:
                setCheckedViewColor(View.INVISIBLE , View.INVISIBLE , View.VISIBLE, View.INVISIBLE);
                if(callback != null){
                    callback.onChangeViewColor(PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_SEPIA);
                }
                break;
            case R.id.cl_more:
                setCheckedViewColor(View.INVISIBLE , View.INVISIBLE , View.INVISIBLE, View.VISIBLE);
                CustomColorModeDialogFragment frag =
                        CustomColorModeDialogFragment.newInstance(PdfViewCtrlSettingsManager.getCustomColorModeBGColor(context),
                        PdfViewCtrlSettingsManager.getCustomColorModeTextColor(context));

                frag.setCustomColorModeSelectedListener((bgColor, txtColor) -> {
                    if (callback != null) {
                        callback.onChangeViewColorCustom(bgColor, txtColor);
                    }
                });
                frag.setStyle(DialogFragment.STYLE_NORMAL, com.pdftron.pdf.tools.R.style.CustomAppTheme);
                FragmentManager fragmentManager = ((DocumentReaderActivity) context).getSupportFragmentManager();
                frag.show(fragmentManager, "custom_color_mode");
                break;

            case R.id.tv_view_mode_selection:
                if(callback != null){
                    this.appPref.setSingleMode(!this.appPref.isDoubleMode());
                    callback.onReadingMode();

                    this.binding.tvViewModeSelection.setText(this.appPref.isDoubleMode()? "Double": "Single");
                    this.binding.ivViewModeSelection2.setVisibility(this.appPref.isDoubleMode()?View.VISIBLE:View.GONE);
                }
                break;

            case R.id.cl_vertical_scroll:
                this.appPref.setVerticalMode(!this.appPref.isVerticalMode());
                this.binding.swVerticalScroll.setChecked(this.appPref.isVerticalMode());

                if(this.callback != null){
                    this.callback.onVerticalScrollMode();
                }
                break;

            case R.id.cl_read_only:

                break;

            case R.id.cl_crop:

                if(callback != null){
                    this.callback.onCropFragmentShow();
                }
                break;
            case R.id.cl_rotate:

                if(this.callback != null){
                    this.callback.onRotateFragmentShow();
                }
                break;

        }
    }

    private void setCheckedViewColor(int light , int dark , int sepia , int more){
        binding.ivLightColorCheck.setVisibility(light);
        binding.ivDarkColorCheck.setVisibility(dark);
        binding.ivSepiaColorCheck.setVisibility(sepia);
        binding.ivMoreColorCheck.setVisibility(more);
    }

    private OnEventToDocumentReader callback;

    public void setCallback(OnEventToDocumentReader callback) {
        this.callback = callback;
    }

    public interface OnEventToDocumentReader{

        void onChangeViewColor(int mode);

        void onChangeViewColorCustom(int bgColor, int txtColor);

        void onVerticalScrollMode();

        void onReadingMode();

        void onCropFragmentShow();

        void onRotateFragmentShow();
    }
}
