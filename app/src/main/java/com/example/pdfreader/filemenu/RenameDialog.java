package com.example.pdfreader.filemenu;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.pdfreader.Constants;
import com.example.pdfreader.R;
import com.example.pdfreader.databinding.RenameDialogLayoutBinding;
import com.example.pdfreader.model.FileModel;
import com.example.pdfreader.utils.FileUtils;
import com.pdftron.pdf.utils.Utils;

public class RenameDialog extends Dialog {

    private RenameDialogLayoutBinding layoutBinding;
    private FileModel fileInfo;
    private OnRenameDialogListener onRenameDialogListener;
    private Context context;
    private boolean isDropbox;

    public RenameDialog(@NonNull Context context, FileModel fileInfo) {
        super(context);
        this.context = context;
        this.fileInfo = fileInfo;
    }

    public void setOnRenameDialogListener(OnRenameDialogListener onRenameDialogListener) {
        this.onRenameDialogListener = onRenameDialogListener;
    }

    public void setDropbox(boolean dropbox) {
        isDropbox = dropbox;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.layoutBinding = RenameDialogLayoutBinding.inflate(LayoutInflater.from(getContext()));
        initDialog(this.layoutBinding.getRoot());
    }

    @SuppressLint("SetTextI18n")
    private void initDialog(View view) {
        this.setCancelable(true);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(view);

        Window window = this.getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.BOTTOM);
            this.layoutBinding.edtRenameFile.requestFocus();
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }


        String fileName = fileInfo.getFilePath().substring(fileInfo.getFilePath().lastIndexOf("/") + 1);
        int extensionLength = Utils.getExtension(fileName).length() + 1;
        this.layoutBinding.edtRenameFile.setText(fileName.substring(0, fileName.length() - extensionLength));
        if (isDropbox) {
            this.layoutBinding.edtRenameFile.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        }
        this.layoutBinding.edtRenameFile.setSelection(this.layoutBinding.edtRenameFile.getText().length());
        this.layoutBinding.tvSaveRename.setOnClickListener(v -> {
            String result = this.layoutBinding.edtRenameFile.getText().toString();
            if (TextUtils.isEmpty(result) || FileUtils.isAllBackSpace(result)) {
                Toast.makeText(context, "File name cannot be empty", Toast.LENGTH_SHORT).show();
                return;
            }

            for (String specialChar : Constants.SPECIAL_CHARS) {
                if (result.contains(specialChar)) {
                    Toast.makeText(context, context.getString(R.string.contain_specific_char), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            String fileTail = "." + Utils.getExtension(fileName);

            if (this.onRenameDialogListener != null) {
//				this.layoutBinding.edtRenameFile.addTextChangedListener(new TextWatcher() {
//					@Override
//					public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//					}
//
//					@Override
//					public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//					}
//
//					@Override
//					public void afterTextChanged(Editable s) {
//						if (layoutBinding.edtRenameFile.getText().toString().contains(" ")) {
//							layoutBinding.edtRenameFile.setText(layoutBinding.edtRenameFile.getText().toString().replaceAll(" ", ""));
//							layoutBinding.edtRenameFile.setSelection(layoutBinding.edtRenameFile.getText().length());
//							Toast.makeText(getContext(), "No Spaces Allowed", Toast.LENGTH_LONG).show();
//						}
//					}
//				});
                this.onRenameDialogListener.onSave(result.trim() + fileTail);
            }
        });

//		InputFilter inputFilter = (source, start, end, dest, dstart, dend) -> {
//			for (int i = 0; i < end; i++) {
//				if (!Character.isLetterOrDigit(source.charAt(i))) { // Accept only letter & digits ; otherwise just return
//					Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();
//					return "";
//				}
//			}
//			return null;
//		};
//
//		this.layoutBinding.edtRenameFile.setFilters(new InputFilter[]{inputFilter});

        this.layoutBinding.tvCancelRename.setOnClickListener(v -> {
            dismiss();
            if (this.onRenameDialogListener != null) {
                this.onRenameDialogListener.onCancel();
            }
        });

    }

    public interface OnRenameDialogListener {
        void onSave(String result);

        void onCancel();
    }
}
