package com.example.pdfreader.filemenu;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.example.pdfreader.R;
import com.example.pdfreader.databinding.DeleteDialogLayoutBinding;

public class DeleteDialog extends Dialog {

    private DeleteDialogLayoutBinding layoutBinding;
    private OnDeleteDialogListener onDeleteDialogListener;

    public DeleteDialog(@NonNull Context context) {
        super(context);
    }

    public void setOnDeleteDialogListener(OnDeleteDialogListener onDeleteDialogListener) {
        this.onDeleteDialogListener = onDeleteDialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.layoutBinding = DeleteDialogLayoutBinding.inflate(LayoutInflater.from(getContext()));
        initDialog(this.layoutBinding.getRoot());
    }

    @SuppressLint("SetTextI18n")
    private void initDialog(View view) {
        this.setCancelable(true);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(view);

        Window window = this.getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.BOTTOM);
        }

        this.layoutBinding.tvYesDelete.setOnClickListener(v -> {
            if (this.onDeleteDialogListener != null) {
                this.onDeleteDialogListener.onDelete();
            }
        });
        this.layoutBinding.tvCancelDelete.setOnClickListener(v -> {
            dismiss();
            if (this.onDeleteDialogListener != null) {
                this.onDeleteDialogListener.onCancel();
            }
        });

    }

    public interface OnDeleteDialogListener {
        void onDelete();

        void onCancel();
    }
}
