package com.example.pdfreader.filemenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pdfreader.databinding.OptionFileItemBinding;
import com.example.pdfreader.model.DocumentOptionModel;

import java.util.List;

public class OptionPDFAdapter extends RecyclerView.Adapter<OptionPDFAdapter.ViewHolder> {

    private Context context;
    private List<DocumentOptionModel> optionFileModelList;
    private OnEventOptionDialog callBack;

    private List<String> favouritePathList;
    private int filePosition;

    public OptionPDFAdapter(Context context, int filePosition) {
        this.context = context;
        this.filePosition = filePosition;
    }

    public void setFavouritePathList(List<String> favouritePathList) {
        this.favouritePathList = favouritePathList;
    }

    public void setOptionFileModelList(List<DocumentOptionModel> optionFileModelList) {
        this.optionFileModelList = optionFileModelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OptionPDFAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        OptionFileItemBinding itemBinding = OptionFileItemBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ViewHolder(itemBinding.getRoot(), itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull OptionPDFAdapter.ViewHolder holder, int index) {
        if (this.optionFileModelList == null) {
            return;
        }
        DocumentOptionModel optionFileModel = optionFileModelList.get(index);
        holder.bindItemView(optionFileModel);
    }

    public void setCallBack(OnEventOptionDialog callBack) {
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return this.optionFileModelList == null ? 0 : this.optionFileModelList.size();
    }

    public interface OnEventOptionDialog {
        void onClickOptionItem(int type , int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private OptionFileItemBinding itemBinding;

        public ViewHolder(@NonNull View itemView, OptionFileItemBinding itemBinding) {
            super(itemView);
            this.itemBinding = itemBinding;
        }

        public void bindItemView(DocumentOptionModel optionFileModel) {
            if (optionFileModel != null) {
                this.itemBinding.ivOptionItem.setImageResource(optionFileModel.getOptionImg());
                this.itemBinding.tvOptionItem.setText(optionFileModel.getOptionName());

                this.itemBinding.ctlOptionItem.setOnClickListener(view -> {
                    if (callBack != null) {
                        callBack.onClickOptionItem(optionFileModel.getOptionType() , filePosition);
                    }
                });
            }
        }
    }
}
