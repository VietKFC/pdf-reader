package com.example.pdfreader.filemenu;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.pdfreader.Constants;
import com.example.pdfreader.R;
import com.example.pdfreader.databinding.DuplicateDialogLayoutBinding;
import com.example.pdfreader.model.FileModel;
import com.example.pdfreader.utils.FileUtils;
import com.pdftron.pdf.utils.Utils;

public class DuplicateDialog extends Dialog {

    private DuplicateDialogLayoutBinding layoutBinding;
    private OnDuplicateListener onDuplicateListener;
    private FileModel fileModel;

    public DuplicateDialog(@NonNull Context context , FileModel fileModel) {
        super(context);
        this.fileModel = fileModel;
    }

    public void setOnDeleteDialogListener(OnDuplicateListener onDeleteDialogListener) {
        this.onDuplicateListener = onDeleteDialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.layoutBinding = DuplicateDialogLayoutBinding.inflate(LayoutInflater.from(getContext()));
        initDialog(this.layoutBinding.getRoot());
    }

    @SuppressLint("SetTextI18n")
    private void initDialog(View view) {
        this.setCancelable(true);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(view);

        Window window = this.getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.BOTTOM);
        }

        String fileName = fileModel.getFilePath().substring(fileModel.getFilePath().lastIndexOf("/") + 1);
        int extensionLength = Utils.getExtension(fileName).length() + 1;
        this.layoutBinding.edtDuplicateFile.setText(fileName.substring(0, fileName.length() - extensionLength));

        this.layoutBinding.edtDuplicateFile.setSelection(this.layoutBinding.edtDuplicateFile.getText().length());
        this.layoutBinding.tvYesDuplicate.setOnClickListener(v -> {
            String result = this.layoutBinding.edtDuplicateFile.getText().toString();
            if (TextUtils.isEmpty(result) || FileUtils.isAllBackSpace(result)) {
                Toast.makeText(getContext(), "File name cannot be empty", Toast.LENGTH_SHORT).show();
                return;
            }

            for (String specialChar : Constants.SPECIAL_CHARS) {
                if (result.contains(specialChar)) {
                    Toast.makeText(getContext(), getContext().getString(R.string.contain_specific_char), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            String fileTail = "." + Utils.getExtension(fileName);

            if (this.onDuplicateListener != null) {
                this.onDuplicateListener.onDuplicate(result.trim() + fileTail);
            }
        });

        this.layoutBinding.tvCancelDuplicate.setOnClickListener(v -> {
            dismiss();
            if (this.onDuplicateListener != null) {
                this.onDuplicateListener.onCancel();
            }
        });

    }

    public interface OnDuplicateListener {
        void onDuplicate(String newPath);

        void onCancel();
    }
}
