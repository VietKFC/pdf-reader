package com.example.pdfreader.filemenu;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.pdfreader.Constants;
import com.example.pdfreader.R;
import com.example.pdfreader.databinding.OptionPdfFileDialogLayoutBinding;
import com.example.pdfreader.model.DocumentOptionModel;
import com.example.pdfreader.model.FileModel;
import com.example.pdfreader.utils.FileUtils;
import com.pdftron.pdf.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OptionPdfFileDialog extends Dialog implements OptionPDFAdapter.OnEventOptionDialog {

    private OptionPdfFileDialogLayoutBinding layoutBinding;
    private OptionPDFAdapter optionPDFFileAdapter;
    private boolean isFromDocumentReader;
    private OnEventBrowseFileFragment callBack;
    private FileModel fileModel;
    private  String oldPath;
    private int position;

    public OptionPdfFileDialog(@NonNull Context context , FileModel fileModel , int position) {
        super(context);
        this.fileModel = fileModel;
        this.position = position;
    }

    @Override
    public void onClickOptionItem(int type, int position) {
        switch (type) {
            case Constants.SHARE:
                FileUtils.shareFile(this.fileModel.getFilePath(), getContext());
                this.dismiss();
                break;
            case Constants.RENAME:
                this.renameDialog(position);
                this.dismiss();
                break;
            case Constants.DUPLICATE:
                duplicateDialog();
                this.dismiss();
                break;
            case Constants.DELETE:
                this.deleteDialog();
                this.dismiss();
                break;
        }
    }

    public interface OnEventBrowseFileFragment {
        void onDeleteFile(int position);

        void onRenameFile(String newName, int position);

        void onDuplicateFile();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.layoutBinding = OptionPdfFileDialogLayoutBinding.inflate(LayoutInflater.from(getContext()));
        initDialog(this.layoutBinding.getRoot());
    }

    @SuppressLint("SetTextI18n")
    private void initDialog(View view) {
        this.setCancelable(true);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(view);

        Window window = this.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawableResource(R.color.transparent);
            window.setGravity(Gravity.BOTTOM);
        }
        this.layoutBinding.ivPdfFile.post(() -> layoutBinding.ivPdfFile.setSelected(true));

        this.layoutBinding.tvPdfFileName.setText(this.fileModel.getFilePath());


        this.layoutBinding.rcvOptionPdfFile.setLayoutManager(new LinearLayoutManager(getContext()));

        File file = new File(fileModel.getFilePath());

        if (file.getAbsolutePath().contains("/storage/emulated/0")) {
            this.layoutBinding.llJump.setVisibility(View.VISIBLE);
            this.layoutBinding.tvJumpFile.setText(file.getAbsolutePath());
        } else {
            this.layoutBinding.llJump.setVisibility(View.GONE);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.layoutBinding.tvBookInfo.setText(sdf.format(file.lastModified()) + " " + getTextForTvFileType());
        this.initAdapter();
        this.layoutBinding.tvPdfFileName.setSelected(true);
    }

    public void initAdapter() {
        this.optionPDFFileAdapter = new OptionPDFAdapter(getContext(), this.position);
        this.optionPDFFileAdapter.setCallBack(this);
        List<DocumentOptionModel> optionFileModelList = new ArrayList<>();

        optionFileModelList.add(new DocumentOptionModel( R.drawable.ic_share, Constants.SHARE , "Share"));
        optionFileModelList.add(new DocumentOptionModel( R.drawable.ic_rename, Constants.RENAME , "Rename"));
        optionFileModelList.add(new DocumentOptionModel(R.drawable.ic_duplicate, Constants.DUPLICATE , "Duplicate"));
        optionFileModelList.add(new DocumentOptionModel(R.drawable.ic_delete, Constants.DELETE , "Delete"));

        optionPDFFileAdapter.setOptionFileModelList(optionFileModelList);
        this.layoutBinding.rcvOptionPdfFile.setAdapter(optionPDFFileAdapter);
    }

    public String getTextForTvFileType() {
        String fileName = fileModel.getFilePath().substring(fileModel.getFilePath().lastIndexOf("/") + 1);
        if (Arrays.asList(com.pdftron.pdf.utils.Constants.FILE_NAME_EXTENSIONS_IMAGE).contains(Utils.getExtension(fileName))) {
            Glide.with(getContext())
                    .load(fileModel.getFilePath())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(layoutBinding.ivPdfFile);

        }
        if (Arrays.asList(com.pdftron.pdf.utils.Constants.FILE_NAME_EXTENSIONS_DOC).contains(Utils.getExtension(fileName))) {
            layoutBinding.ivPdfFile.setImageResource(R.drawable.ic_file_doc);
        }
        return String.format(getContext().getString(R.string.file_infor),
                fileModel.getMimeType() ,
                FileUtils.convertBytes((long)fileModel.getSize()));
    }

    public void setCallBack(OnEventBrowseFileFragment callBack) {
        this.callBack = callBack;
    }

    public void renameDialog(int position) {
        this.oldPath = this.fileModel.getFilePath();
        RenameDialog renameDialog = new RenameDialog(this.getContext(), fileModel);
        renameDialog.setOnRenameDialogListener(new RenameDialog.OnRenameDialogListener() {
            @Override
            public void onSave(String result) {
                if (!TextUtils.isEmpty(result)) {
                    String newPath = FileUtils.renameFile(getContext(), result, oldPath);
                    if (TextUtils.isEmpty(newPath) || newPath.startsWith(" ")) {
                        return;
                    }

                    File newFile = new File(newPath);
                    if (callBack != null) {
                        callBack.onRenameFile(result, position);
                    }
                    renameDialog.dismiss();
                }
            }

            @Override
            public void onCancel() {

            }
        });
        renameDialog.show();
    }

    public void deleteDialog() {
        String oldPath = this.fileModel.getFilePath();
        DeleteDialog deleteDialog = new DeleteDialog(getContext());
        deleteDialog.setOnDeleteDialogListener(new DeleteDialog.OnDeleteDialogListener() {
            @Override
            public void onDelete() {
                if (FileUtils.deleteFile(oldPath)) {
                    if (callBack != null) {
                        callBack.onDeleteFile(position);
                    }
                    getContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(oldPath))));
                }
                deleteDialog.dismiss();
            }

            @Override
            public void onCancel() {

            }
        });
        deleteDialog.show();
    }

    public void duplicateDialog() {
        DuplicateDialog duplicateDialog = new DuplicateDialog(getContext() , fileModel);
        duplicateDialog.setOnDeleteDialogListener(new DuplicateDialog.OnDuplicateListener() {
            @Override
            public void onDuplicate(String newPath) {
                try {
                    FileUtils.copy(new File(fileModel.getFilePath()) ,
                            new File(new File(fileModel.getFilePath()).getParent() , newPath) , getContext());
                    callBack.onDuplicateFile();
                    duplicateDialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancel() {

            }
        });
        duplicateDialog.show();
    }
}
