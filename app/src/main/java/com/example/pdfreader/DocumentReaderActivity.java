package com.example.pdfreader;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.pdfreader.adapter.AnnotationAdapter;
import com.example.pdfreader.databinding.ActivityDocumentReaderBinding;
import com.example.pdfreader.model.AnnotModel;
import com.example.pdfreader.texttospeech.TextToSpeechUtil;
import com.example.pdfreader.utils.AppPref;
import com.example.pdfreader.utils.documentutils.EditPdfManager;
import com.example.pdfreader.utils.documentutils.ToolUtils;
import com.example.pdfreader.viewmode.ViewModeDialog;
import com.pdftron.common.PDFNetException;
import com.pdftron.filters.MappedFile;
import com.pdftron.pdf.DocumentConversion;
import com.pdftron.pdf.OfficeToPDFOptions;
import com.pdftron.pdf.PDFDoc;
import com.pdftron.pdf.PDFRasterizer;
import com.pdftron.pdf.PDFViewCtrl;
import com.pdftron.pdf.config.ToolManagerBuilder;
import com.pdftron.pdf.tools.ToolManager;
import com.pdftron.pdf.utils.PdfViewCtrlSettingsManager;
import com.pdftron.pdf.utils.Utils;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DocumentReaderActivity extends AppCompatActivity implements View.OnClickListener , AnnotationEventCallBack {

    private ActivityDocumentReaderBinding binding;
    private String filePath;
    private PDFDoc pdfDoc;
    private boolean isPdfFile;
    private EditPdfManager editPdfManager;
    private ToolManager mToolManager;
    private AppPref appPref;
    private List<AnnotModel> annotModelList;
    private AnnotationAdapter adapter;
    private TextToSpeechUtil ttsUtils;
    private boolean isPlayTts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = ActivityDocumentReaderBinding.inflate(getLayoutInflater());
        setContentView(this.binding.getRoot());
        this.filePath = getIntent().getStringExtra("pdf_path_key");
        if (this.filePath != null) {
            this.isPdfFile = this.filePath.substring(this.filePath.lastIndexOf(".")).contains("pdf");
        }
        this.appPref = AppPref.instance(this);

        initView();
        loadPdfView(filePath);
        initTool();
        this.editPdfManager = new EditPdfManager(this.binding.pdfViewCtrl, this.binding, this.pdfDoc);
    }

    private void initTool() {
        ToolManagerBuilder toolManagerBuilder = ToolManagerBuilder.from().setUseDigitalSignature(true);
        this.mToolManager = toolManagerBuilder.build(this, this.binding.pdfViewCtrl);
        this.mToolManager.setCanResumePdfDocWithoutReloading(true);
        this.mToolManager.setSnappingEnabledForMeasurementTools(true);

        PdfViewCtrlSettingsManager.setColorMode(DocumentReaderActivity.this, PdfViewCtrlSettingsManager.getColorMode(this));
        updateColorMode();

        ToolUtils.initThumbnailSlider(this.binding.thumbnailSlider, this.binding.pdfViewCtrl, this, getSupportFragmentManager());

        this.setViewReading();

        this.ttsUtils = new TextToSpeechUtil(this.pdfDoc , this.binding.pdfViewCtrl , this);
        ttsUtils.setListener(new TextToSpeechUtil.OnChangeSpeakProgressListener() {
            @Override
            public void onChangeProgress(int position) {
                binding.progressbarAudio.setProgress(position);
            }

            @Override
            public void onChangePage(int totalLine) {
                binding.progressbarAudio.setMax(totalLine);
            }
        });
    }

    private void initView() {
        this.binding.ivBack.setOnClickListener(this);
        this.binding.ivSearchText.setOnClickListener(this);
        this.binding.ivViewMode.setOnClickListener(this);
        this.binding.fabAnnot.setOnClickListener(this);
        this.binding.clUndoRedo.ivCloseEdit.setOnClickListener(this);
        this.binding.clUndoRedo.ivSaveEdit.setOnClickListener(this);
        this.binding.clUndoRedo.ivRedo.setOnClickListener(this);
        this.binding.clUndoRedo.ivUndo.setOnClickListener(this);
        this.binding.clAnnot.ivPan.setOnClickListener(this);
        this.binding.clAnnot.ivEraser.setOnClickListener(this);
        this.binding.ivTts.setOnClickListener(this);
        this.binding.closeAudioBarIv.setOnClickListener(this);
        this.binding.pauseResumeAudio.setOnClickListener(this);
        this.binding.nextAudioIv.setOnClickListener(this);
        this.binding.backAudioIv.setOnClickListener(this);
        this.binding.settingAudioIv.setOnClickListener(this);
        this.binding.clAnnot.ivPan.setOnClickListener(this);
        this.binding.clAnnot.ivEraser.setOnClickListener(this);

        this.binding.clAnnot.rvAnnot.setLayoutManager(
                new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false));
        this.adapter = new AnnotationAdapter(this);
        this.adapter.setCallBack(this);
        this.annotModelList = new ArrayList<>();
        ToolUtils.initAnnotationData(this.annotModelList, this);
        this.adapter.setAnnotList(this.annotModelList);
        this.binding.clAnnot.rvAnnot.setAdapter(this.adapter);

        this.binding.pdfViewCtrl.addPageChangeListener((i, i1, pageChangeState) -> {
            binding.progressbarAudio.setProgress(0);
            ttsUtils.onPageChange( (ttsUtils.isReading()) ? true : false);
        });

    }

    private void loadPdfView(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return;
        }
        Uri uri = Uri.parse(filePath);
        if (filePath.startsWith("content://")) {
            uri = Uri.parse(filePath);
        } else if (filePath.startsWith("file://")) {
            uri = Uri.fromFile(new File(filePath));
        }

        try {
            if (this.isPdfFile) {
                this.pdfDoc = this.binding.pdfViewCtrl.openPDFUri(uri, null);
            } else {
                OfficeToPDFOptions options = new OfficeToPDFOptions("{\"RemovePadding\": true}");
                DocumentConversion documentConversion = this.binding.pdfViewCtrl.openNonPDFUri(uri, options);
                try {
                    this.pdfDoc = documentConversion.getDoc();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (PDFNetException | FileNotFoundException e) {
            e.printStackTrace();
        }
        if(this.isPdfFile && this.pdfDoc == null) {
            try {
                pdfDoc = new PDFDoc(this.filePath);
            } catch (PDFNetException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.iv_search_text:
                this.editPdfManager.initSearchText(this.binding.searchtoolbar, this.binding.findTextView);
                break;

            case R.id.iv_view_mode:
                ViewModeDialog dialog = new ViewModeDialog(this);
                initViewModeDialog(dialog);
                dialog.show();
                break;

            case R.id.fab_annot:
                setVisibleViewWhenEdit(View.VISIBLE, View.GONE);
                setPdfViewConstraintSet(R.id.card_annot);
                break;

            case R.id.iv_close_edit:
                setVisibleViewWhenEdit(View.GONE, View.VISIBLE);
                setPdfViewConstraintSet(R.id.thumbnail_slider);
                this.editPdfManager.resetEditTool(this.mToolManager);
                break;

            case R.id.iv_save_edit:
                Toast.makeText(this , getString(R.string.text_save_success) , Toast.LENGTH_SHORT).show();
                setVisibleViewWhenEdit(View.GONE, View.VISIBLE);
                setPdfViewConstraintSet(R.id.thumbnail_slider);
                this.editPdfManager.resetEditTool(this.mToolManager);
                this.editPdfManager.commitAdvanceShapeTool(this.mToolManager);
                this.editPdfManager.saveIncrementally();
                break;

            case R.id.iv_tts:
                this.binding.ttsAudioCtl.setVisibility(View.VISIBLE);
                this.binding.fabAnnot.setVisibility(View.INVISIBLE);
                this.ttsUtils.startSpeak(0);
                break;

            case R.id.close_audio_bar_iv:
                this.binding.ttsAudioCtl.setVisibility(View.GONE);
                this.binding.fabAnnot.setVisibility(View.VISIBLE);
                this.ttsUtils.stopSpeak();
                break;

            case R.id.pause_resume_audio:
                if(!isPlayTts) {
                    this.ttsUtils.pauseSpeak();
                    this.binding.pauseResumeAudio.setImageResource(R.drawable.ic_pause_audio);
                    isPlayTts = true;
                }else {
                    this.ttsUtils.resumeSpeak();
                    this.binding.pauseResumeAudio.setImageResource(R.drawable.ic_resume_audio);
                    isPlayTts = false;
                }
                break;

            case R.id.next_audio_iv:
                this.ttsUtils.nextLineReader();
                break;

            case R.id.back_audio_iv:
                this.ttsUtils.backLineReader();
                break;

            case R.id.setting_audio_iv:
                this.ttsUtils.showAudioSettingDialog();
                break;

            case R.id.iv_pan:
                this.binding.clAnnot.ivPan.setColorFilter(getResources().getColor(R.color.colorRed));
                this.binding.clAnnot.ivEraser.setColorFilter(getResources().getColor(R.color.black));
                this.binding.clAnnot.tvPan.setTextColor(getResources().getColor(R.color.colorRed));
                this.binding.clAnnot.tvEraser.setTextColor(getResources().getColor(R.color.black));
                ToolUtils.setToolType(this.mToolManager , Constants.PAN_TOOL);
                for (int i = 0; i < annotModelList.size(); i++) {
                    annotModelList.get(i).setSelected(false);
                }
                adapter.setAnnotList(annotModelList);
                break;

            case R.id.iv_eraser:
                this.binding.clAnnot.ivPan.setColorFilter(getResources().getColor(R.color.black));
                this.binding.clAnnot.ivEraser.setColorFilter(getResources().getColor(R.color.colorRed));
                this.binding.clAnnot.tvPan.setTextColor(getResources().getColor(R.color.black));
                this.binding.clAnnot.tvEraser.setTextColor(getResources().getColor(R.color.colorRed));
                ToolUtils.setToolType(this.mToolManager , Constants.ERASER_TOOL);
                for (int i = 0; i < annotModelList.size(); i++) {
                    annotModelList.get(i).setSelected(false);
                }
                adapter.setAnnotList(annotModelList);
                break;

            case R.id.iv_undo:
                ToolUtils.undoRedoAction(this.binding.pdfViewCtrl , true);
                break;

            case R.id.iv_redo:
                ToolUtils.undoRedoAction(this.binding.pdfViewCtrl , false);
                break;
        }
    }

    private void setVisibleViewWhenEdit(int editShow, int editGone) {
        this.binding.llUndoRedo.setVisibility(editShow);
        this.binding.cardAnnot.setVisibility(editShow);
        this.binding.fabAnnot.setVisibility(editGone);
        this.binding.clToolbar.setVisibility(editGone);
    }

    private void setPdfViewConstraintSet(int id) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this.binding.clParent);
        constraintSet.connect(R.id.pdf_view_ctrl, ConstraintSet.BOTTOM, id, ConstraintSet.TOP);
        constraintSet.applyTo(this.binding.clParent);
    }

    private void initViewModeDialog(ViewModeDialog dialog) {
        dialog.setCallback(new ViewModeDialog.OnEventToDocumentReader() {
            @Override
            public void onChangeViewColor(int mode) {
                PdfViewCtrlSettingsManager.setColorMode(DocumentReaderActivity.this, mode);
                updateColorMode();
            }

            @Override
            public void onChangeViewColorCustom(int bgColor, int txtColor) {
                PdfViewCtrlSettingsManager.setCustomColorModeTextColor(DocumentReaderActivity.this, txtColor);
                PdfViewCtrlSettingsManager.setCustomColorModeBGColor(DocumentReaderActivity.this, bgColor);
                PdfViewCtrlSettingsManager.setColorMode(DocumentReaderActivity.this, PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_CUSTOM);
                updateColorMode();
            }

            @Override
            public void onVerticalScrollMode() {
                setViewReading();
            }

            @Override
            public void onReadingMode() {
                setViewReading();
            }

            @Override
            public void onCropFragmentShow() {
                editPdfManager.showCropDialog(getSupportFragmentManager());
            }

            @Override
            public void onRotateFragmentShow() {
                editPdfManager.showRotateDialog(getSupportFragmentManager());
            }
        });
    }

    public void setViewReading() {
        if (appPref.isVerticalMode()) {
            binding.pdfViewCtrl.setPagePresentationMode(this.appPref.isDoubleMode() ? PDFViewCtrl.PagePresentationMode.FACING_CONT : PDFViewCtrl.PagePresentationMode.SINGLE_CONT);
        } else {
            binding.pdfViewCtrl.setPagePresentationMode(this.appPref.isDoubleMode() ? PDFViewCtrl.PagePresentationMode.FACING : PDFViewCtrl.PagePresentationMode.SINGLE);
        }
    }

    private void updateColorMode() {
        int colorMode = PdfViewCtrlSettingsManager.getColorMode(this);
        int clientBackgroundColor = PDFViewCtrl.DEFAULT_BG_COLOR;
        int mode = PDFRasterizer.e_postprocess_none;
        int customBGColor = 0;
        int customTxtColor = 0;
        switch (colorMode) {
            case PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_CUSTOM:
                customBGColor = PdfViewCtrlSettingsManager.getCustomColorModeBGColor(this);
                customTxtColor = PdfViewCtrlSettingsManager.getCustomColorModeTextColor(this);
                clientBackgroundColor = ToolUtils.getViewerBackgroundColor(customBGColor);
                mode = PDFRasterizer.e_postprocess_gradient_map;
                break;
            case PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_SEPIA:
                mode = PDFRasterizer.e_postprocess_gradient_map;
                InputStream is = null;
                OutputStream os = null;
                try {
                    File filterFile = new File(getCacheDir(), "sepia_mode_filter.png");
                    if (!filterFile.exists() || !filterFile.isFile()) {
                        is = getResources().openRawResource(com.pdftron.pdf.tools.R.raw.sepia_mode_filter);
                        os = new FileOutputStream(filterFile);
                        IOUtils.copy(is, os);
                    }
                    binding.pdfViewCtrl.setColorPostProcessMapFile(new MappedFile(filterFile.getAbsolutePath()));
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Utils.closeQuietly(is);
                    Utils.closeQuietly(os);
                }
                break;
            case PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_NIGHT:
                clientBackgroundColor = PDFViewCtrl.DEFAULT_DARK_BG_COLOR;
                mode = PDFRasterizer.e_postprocess_night_mode;
                break;
        }

        try {
            this.binding.pdfViewCtrl.setClientBackgroundColor(
                    Color.red(clientBackgroundColor),
                    Color.green(clientBackgroundColor),
                    Color.blue(clientBackgroundColor), false);
            this.binding.pdfViewCtrl.setColorPostProcessMode(mode);
            if (colorMode == PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_CUSTOM) {
                this.binding.pdfViewCtrl.setColorPostProcessColors(customBGColor, customTxtColor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        mToolManager.setNightMode(isNightModeForToolManager());
    }

    protected boolean isNightModeForToolManager() {
        return (PdfViewCtrlSettingsManager.getColorMode(this) == PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_NIGHT
                || (PdfViewCtrlSettingsManager.getColorMode(this) == PdfViewCtrlSettingsManager.KEY_PREF_COLOR_MODE_CUSTOM
                && Utils.isColorDark(PdfViewCtrlSettingsManager.getCustomColorModeBGColor(this))));
    }

    @Override
    public void onClickAnnotation(AnnotModel annotModel) {
        ToolUtils.setToolType(this.mToolManager , annotModel.getAnnotType());
        for (int i = 0; i < annotModelList.size(); i++) {
            if(annotModelList.get(i).getAnnotType() == annotModel.getAnnotType()) {
                annotModelList.get(i).setSelected(true);
            }else{
                annotModelList.get(i).setSelected(false);
            }
        }
        adapter.setAnnotList(this.annotModelList);
        this.binding.clAnnot.ivPan.setColorFilter(getResources().getColor(R.color.black));
        this.binding.clAnnot.ivEraser.setColorFilter(getResources().getColor(R.color.black));
        this.binding.clAnnot.tvPan.setTextColor(getResources().getColor(R.color.black));
        this.binding.clAnnot.tvEraser.setTextColor(getResources().getColor(R.color.black));
    }
}
