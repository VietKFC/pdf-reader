package com.example.pdfreader;

import com.example.pdfreader.model.AnnotModel;

public interface AnnotationEventCallBack {

    void onClickAnnotation(AnnotModel annotModel);
}
