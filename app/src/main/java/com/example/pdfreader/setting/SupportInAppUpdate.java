package com.example.pdfreader.setting;

import android.app.Activity;
import android.content.IntentSender;
import android.view.View;

import com.example.pdfreader.R;
import com.example.pdfreader.utils.AppPref;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

public class SupportInAppUpdate {
    public static final int UPDATE_REQUEST_CODE = 63879;

    private int mType;

    private View mView;
    private Activity mActivity;
    private AppUpdateManager updateManager;
    private Task<AppUpdateInfo> updateInfoTask;

    public SupportInAppUpdate(Activity activity, View viewContainer) {
        this.mActivity = activity;
        this.mView = viewContainer;
        this.mType = AppPref.instance(mActivity).getInAppUpdateType();
    }

    public void configInAppUpdate() {
        this.updateManager = AppUpdateManagerFactory.create(this.mActivity);
        this.updateInfoTask = this.updateManager.getAppUpdateInfo();
    }

    public void addOnSuccessListener(OnSuccessListener onSuccessListener) {
        if (onSuccessListener == null || this.updateInfoTask == null) {
            return;
        }
        this.updateInfoTask.addOnSuccessListener(result -> {
            if (onSuccessListener != null) {
                onSuccessListener.onSucces(result);
            }
        });
    }

    public void startUpdate(AppUpdateInfo appUpdateInfo) {
        if (this.updateInfoTask != null) {
            try {
                this.updateManager.startUpdateFlowForResult(appUpdateInfo, this.mType, this.mActivity, UPDATE_REQUEST_CODE);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        }
    }

    public void checkUpdateDownLoaded() {
        if (this.updateManager != null) {
            this.updateManager.getAppUpdateInfo().addOnSuccessListener(appUpdateInfo -> {
                if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                    this.popupSnackbarForCompleteUpdate();
                }

                if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                    try {
                        this.updateManager.startUpdateFlowForResult(appUpdateInfo, this.mType, this.mActivity, UPDATE_REQUEST_CODE);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void popupSnackbarForCompleteUpdate() {
        if (this.mView == null) return;
        try {
            Snackbar snackbar = Snackbar.make(this.mView, "An update has just been downloaded.", Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("RESTART", view -> this.updateManager.completeUpdate());
            snackbar.setActionTextColor(this.mActivity.getResources().getColor(R.color.yellow));
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnSuccessListener {
        void onSucces(AppUpdateInfo appUpdateInfo);
    }
}
