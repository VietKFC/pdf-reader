package com.example.pdfreader.setting;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pdfreader.Constants;
import com.example.pdfreader.R;
import com.example.pdfreader.databinding.ActivitySettingBinding;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivitySettingBinding activitySettingBinding;
    private View decorView;
    private SupportInAppUpdate supportInAppUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activitySettingBinding = ActivitySettingBinding.inflate(getLayoutInflater());
        View view = this.activitySettingBinding.getRoot();
        setContentView(view);

        decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(visibility -> {
            if (visibility == 0) {
                decorView.setSystemUiVisibility(hideSystemUI());
            }
        });
        this.initView();
        this.supportInAppUpdate = new SupportInAppUpdate(SettingActivity.this, this.activitySettingBinding.getRoot());
        supportInAppUpdate.checkUpdateDownLoaded();
    }

    public int hideSystemUI() {
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            decorView.setSystemUiVisibility(hideSystemUI());
        }
    }

    private void initView() {
        this.activitySettingBinding.ivBack.setOnClickListener(this);

        this.activitySettingBinding.clPrivacy.setOnClickListener(this);
        this.activitySettingBinding.clCheckUpdate.setOnClickListener(this);
        this.activitySettingBinding.clLeaveFeedback.setOnClickListener(this);
        this.activitySettingBinding.clRate.setOnClickListener(this);
    }

    private void checkUpdate() {
        this.supportInAppUpdate.configInAppUpdate();
        this.supportInAppUpdate.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.availableVersionCode() > Constants.getAppVersionCode(this)) {
                this.supportInAppUpdate.startUpdate(appUpdateInfo);
                return;
            }
            Toast.makeText(this, "This is the latest version", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.supportInAppUpdate != null) {
            this.supportInAppUpdate.checkUpdateDownLoaded();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.cl_check_update:
                checkUpdate();
                //todo check update
                break;
            case R.id.cl_leave_feedback:
                sendEmail();
                break;
            case R.id.cl_rate:
                openMarket(this);
                break;
        }
    }

    public static void openMarket(Context context) {
        String packageName = context.getPackageName();
        if (TextUtils.isEmpty(packageName)) {
            return;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ActivityNotFoundException anfe) {
            Intent intent2 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent2);
        }
    }

    public void sendEmail() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
//			int longVersionCode = (int) pInfo.getLongVersionCode();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String device = Build.MANUFACTURER.toUpperCase() + " " + android.os.Build.MODEL;
        String release = Build.VERSION.RELEASE;
        String content = device + " .Version: " + release + ". Bookify Version: ";
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"vietngo1000@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback from Bookify");
        emailIntent.putExtra(Intent.EXTRA_TEXT, content);
        Intent chooser = Intent.createChooser(emailIntent, "Mail to ..");
        if (emailIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(chooser);
        }
    }
}