package com.example.pdfreader.splash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.pdfreader.R;
import com.example.pdfreader.databinding.FragmentOnBoardingBinding;

public class OnBoardingFragment extends Fragment {

    private static final String ONBOARDING_BUNDLE_STEP_KEY = "onboarding_bundle_step_key";

    private FragmentOnBoardingBinding onBoardingBinding;
    private int step;

    public static OnBoardingFragment newInstance(int step) {
        OnBoardingFragment onBoardingFragment = new OnBoardingFragment();
        final Bundle b = new Bundle(1);
        b.putInt(ONBOARDING_BUNDLE_STEP_KEY, step);
        onBoardingFragment.setArguments(b);
        return onBoardingFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.step = getArguments().getInt(ONBOARDING_BUNDLE_STEP_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.onBoardingBinding = FragmentOnBoardingBinding.inflate(inflater, container, false);
        View view = this.onBoardingBinding.getRoot();
        initView();
        return view;
    }

    private void initView() {
        int imageRes;
        String title;
        String description;
        switch (step) {
            default:
            case 1:
                imageRes = R.drawable.im_onboarding_first;
                title = "PDF Reader";
                description = getString(R.string.intro_spl_1);
                break;
            case 2:
                imageRes = R.drawable.im_onboarding_second;
                title = getString(R.string.all_in_one_pdf);
                description = getString(R.string.intro_spl_2);
                break;

            case 3:
                imageRes = R.drawable.im_onboarding_third;
                title = getString(R.string.convert_text_to_speech);
                description = getString(R.string.intro_spl_3);
                break;
        }

        this.onBoardingBinding.ivOnboardingHeader.setImageResource(imageRes);
        this.onBoardingBinding.tvOnboardingTitle.setText(title);
        this.onBoardingBinding.tvOnboardingContent.setText(description);
    }
}
