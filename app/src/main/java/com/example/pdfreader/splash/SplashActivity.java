package com.example.pdfreader.splash;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pdfreader.R;
import com.example.pdfreader.databinding.ActivitySplashBinding;

import java.util.Collections;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private ActivitySplashBinding activitySplashBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.activitySplashBinding = ActivitySplashBinding.inflate(getLayoutInflater());

        Window window = SplashActivity.this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        List<String> premiumSkus = Collections.singletonList("junk");

        View view = this.activitySplashBinding.getRoot();
        setContentView(view);

        this.activitySplashBinding.tvSplashAction.setOnClickListener(v -> {
            startNextActivity();
        });
        changeColorSpannableText();
    }

    private void startNextActivity() {
        Intent intent = new Intent(SplashActivity.this, OnBoardingActivity.class);
        startActivity(intent);
        finish();
    }

    public void changeColorSpannableText() {
        String policyTitle = this.getResources().getString(R.string.splash_policy_title);
        String privacyPolicy = this.getResources().getString(R.string.privacy_policy);

        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(policyTitle).append(" ");
        builder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorGrey3)), 0, builder.length() - 1, 0);

        builder.append(privacyPolicy.trim());
        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
            }
        }, builder.length() - privacyPolicy.trim().length(), builder.length(), 0);

        this.activitySplashBinding.tvSplashPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        this.activitySplashBinding.tvSplashPolicy.setHighlightColor(0);
        this.activitySplashBinding.tvSplashPolicy.setText(builder, TextView.BufferType.SPANNABLE);
    }
}