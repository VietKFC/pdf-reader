package com.example.pdfreader.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.pdfreader.MainActivity;
import com.example.pdfreader.R;
import com.example.pdfreader.databinding.ActivityOnBoardingBinding;
import com.example.pdfreader.utils.AppPref;

public class OnBoardingActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityOnBoardingBinding onBoardingBinding;
    private int stepCount = 1;
    private Handler handler;
    private Runnable runnable;
    private View decorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(visibility -> {
            if (visibility == 0) {
                decorView.setSystemUiVisibility(hideSystemUI());
            }
        });

        AppPref instance = AppPref.instance(this);
        if (instance.isSkipOnBoarding()) {
            startNextActivity();
            return;
        }

        this.onBoardingBinding = ActivityOnBoardingBinding.inflate(getLayoutInflater());
        View view = this.onBoardingBinding.getRoot();
        setContentView(view);


        initView();
    }


    public int hideSystemUI() {
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            decorView.setSystemUiVisibility(hideSystemUI());
        }
    }

    private void initView() {
        SplashPagerAdapter splashPagerAdapter = new SplashPagerAdapter(this.getSupportFragmentManager());
        this.onBoardingBinding.vpSplash.setAdapter(splashPagerAdapter);
        this.onBoardingBinding.vpSplash.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                stepCount = position + 1;
                switch (position) {
                    case 0:
                        onBoardingBinding.viewThreeDot.ivDot1.setImageResource(R.drawable.ic_red_dot);
                        onBoardingBinding.viewThreeDot.ivDot2.setImageResource(R.drawable.ic_grey_dot);
                        onBoardingBinding.viewThreeDot.ivDot3.setImageResource(R.drawable.ic_grey_dot);
                        onBoardingBinding.tvNextSpl.setText("Next");
                        break;
                    case 1:
                        onBoardingBinding.viewThreeDot.ivDot1.setImageResource(R.drawable.ic_grey_dot);
                        onBoardingBinding.viewThreeDot.ivDot2.setImageResource(R.drawable.ic_red_dot);
                        onBoardingBinding.viewThreeDot.ivDot3.setImageResource(R.drawable.ic_grey_dot);
                        onBoardingBinding.tvNextSpl.setText("Next");
                        break;
                    case 2:
                        onBoardingBinding.viewThreeDot.ivDot1.setImageResource(R.drawable.ic_grey_dot);
                        onBoardingBinding.viewThreeDot.ivDot2.setImageResource(R.drawable.ic_grey_dot);
                        onBoardingBinding.viewThreeDot.ivDot3.setImageResource(R.drawable.ic_red_dot);
                        onBoardingBinding.tvNextSpl.setText("Let 's go");
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        this.onBoardingBinding.tvNextSpl.setOnClickListener(this);
        this.initAutoNextStep();
    }

    private void initAutoNextStep() {
        this.handler = new Handler();
        this.runnable = () -> {
            if (stepCount < 3) {
                this.onBoardingBinding.vpSplash.setCurrentItem(stepCount);
                if (handler != null && runnable != null) {
                    handler.postDelayed(runnable, 2500);
                }
            }
        };
        this.handler.postDelayed(this.runnable, 2500);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_next_spl) {
            if (stepCount >= 3) {
                this.startNextActivity();
                return;
            }
            if (this.handler != null && runnable != null) {
                this.handler.removeCallbacksAndMessages(null);
                this.handler.postDelayed(this.runnable, 2500);
            }

            this.onBoardingBinding.vpSplash.setCurrentItem(stepCount);
        }
    }

    private void startNextActivity() {
        AppPref.instance(this).setSkipOnBoarding(true);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (this.handler != null) {
            this.handler.removeCallbacksAndMessages(null);
        }
    }

    public static class SplashPagerAdapter extends FragmentStatePagerAdapter {

        public SplashPagerAdapter(@NonNull FragmentManager fm) {
            super(fm, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            int step = position + 1;
            return OnBoardingFragment.newInstance(step);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}